## Instalación de Empresa Libre


<BR>
### Actualización

<BR>
<div class="alert-box notice"><span>TIP: </span>
    Es <b>muy importante</b> y es <b>tu responsabilidad</b> mantener siempre actualizado y al día el sistema.
</div>

<BR><BR>
Mantener al día el sistema te permite tener siempre los ultimos cambios realizados
en el, recibir las correcciones de errores y las mejoras que vamos incorporando.

Siempre consulta el [historial de cambios](/notas) para saber si hay algún proceso
extra que seguir al actualizar. Es muy importante que revises cada nota desde
tu versión actual y hasta la más reciente.

<BR>
Al iniciar el sistema, debes de ver la versión actual del mismo. Si no la ves,
es que tienes una versión demasiado desactualizada, es importante actualices a
la brevedad.

![Versión del sistema](img/01/install_001.png)


<BR>
Ya dentro del sistema, el proceso para actualizar es:

1. Entra a la carpeta del proyecto.

        ┌─[empresalibre-][empresa]->{~}
        └──> cd proyectos/empresa-libre
        ┌─[empresalibre-][empresa]->{~/proyectos/empresa-libre}
        └──>

1. Actualiza la rama `master` del repositorio. El resultado variará, dependiendo desde la versión que estes actualizando, entre más vieja sea, veras más archivos modificados.

        └──> git pull origin master
        remote: Enumerating objects: 197, done.
        remote: Counting objects: 100% (197/197), done.
        remote: Compressing objects: 100% (93/93), done.
        remote: Total 197 (delta 135), reused 146 (delta 99)
        Recibiendo objetos: 100% (197/197), 131.71 KiB | 956.00 KiB/s, listo.
        Resolviendo deltas: 100% (135/135), listo.
        Desde https://gitlab.com/mauriciobaeza/empresa-libre
         * branch            master     -> FETCH_HEAD
           f0ab924..18b1880  master     -> origin/master
        Actualizando f0ab924..18b1880
        Fast-forward
         CHANGELOG.md                            |  44 ++++++++
         VERSION                                 |   2 +-
         source/app/controllers/main.py          |   3 +-
         source/app/controllers/util.py          |  35 +++++-
         source/app/models/db.py                 |  23 +++-
         source/app/models/main.py               | 278 ++++++++++++++++++++++++++++++++++++++--------
         source/app/settings.py                  |   3 +-
         source/static/css/app.css               |   7 ++
         source/static/js/controller/admin.js    |  44 +++++++-
         source/static/js/controller/bancos.js   |  89 +++++++++++++--
         source/static/js/controller/partners.js | 163 ++++++++++++++++++---------
         source/static/js/controller/util.js     |   1 +
         source/static/js/ui/admin.js            |  18 ++-
         source/static/js/ui/bancos.js           |   9 +-
         source/static/js/ui/main.js             |   5 +-
         source/static/js/ui/partners.js         |  28 ++++-
         source/templates/base.html              |   2 +-
         17 files changed, 626 insertions(+), 128 deletions(-)

1. Si las [notas de lanzamiento](/notas) te indican que debes de hacer algún otro proceso, generalmente será que migres la base de datos.
    1. ¿Cuando debo migrar la base de datos?
        1. Si te lo indica la **ultima** nota de lanzamiento
        1. Si te lo indica alguna nota intermedia desde tu versión actual y hasta la ultima.
        1. Si **no ves** la versión de **Empresa Libre** al inicio del sistema
        1. Si **no** estas seguro desde que versión estas actualizando.
    * Para migrar entra a la siguiente carpeta

            └──> cd source/app/models/
            ┌─[empresalibre-][empresa]->{~/proyectos/empresa-libre/source/app/models}
            └──>

    * **IMPORTANTE:** siempre saca un respaldo de tu base de datos **antes de migrar**

            └──> python main.py -bk
            [21-Sep-2018 23:34:08] INFO: API: Generando backup de: LAN7008173R5
            [21-Sep-2018 23:34:09] INFO: API:   Backup generado de LAN7008173R5
            [21-Sep-2018 23:34:09] INFO: API:   Sin datos para sincronización particular de lan7008173r5.bk

    * Si en vez del mensaje anterior, ves un mensaje de error como el siguiente. Mira en [errores más comúnes][1] para arreglarlo primero. Una vez resuelto, vuelve a ejecutar el comando anterior. **Verifica siempre estar en la carpeta correcta**.

              File "/usr/lib/python3.7/site-packages/requests/adapters.py", line 513, in send
                raise ConnectionError(e, request=request)
            requests.exceptions.ConnectionError: HTTPSConnectionPool(host='seafile.empresalibre.net', port=443): Max retries exceeded with url: /api2/auth-token/ (Caused by NewConnectionError('<urllib3.connection.VerifiedHTTPSConnection object at 0x7f2f031a4550>: Failed to establish a new connection: [Errno -2] Name or service not known'))

    * **IMPORTANTE:** Si **no sabes** desde que versión estas actualizando, o no has actualizado desde la **versión 1.2.0 del 18 de diciembre del 2017**. Entonces, primero usa el siguente comando.

            └──> python main.py -bd
            Introduce el RFC: TU_RFC

    * Ahora si, si ya **no obtienes ningún error** y estas al día, puedes migrar tu base de datos con el comando.

            └──> python main.py -m -r TU_RFC

    * Por supuesto, reemplaza **TU_RFC** por el RFC del emisor a actualizar.

            └──> python main.py -m -r LAN7008173R5
            [21-Sep-2018 23:41:55] INFO: API: Conectado a la BD...
            [21-Sep-2018 23:41:55] INFO: API: Creando tablas nuevas...
            [21-Sep-2018 23:41:55] INFO: API: Tablas creadas correctamente...
            [21-Sep-2018 23:41:55] INFO: API: Iniciando migración de tablas...
            [21-Sep-2018 23:41:55] INFO: API: Tablas migradas correctamente...
            [21-Sep-2018 23:41:55] INFO: API: Importando datos...
            [21-Sep-2018 23:41:55] INFO: API:   Importando tabla: Categorias
            [21-Sep-2018 23:41:55] INFO: API:   Importando tabla: SATImpuestos
            [21-Sep-2018 23:41:55] INFO: API:   Importando tabla: SATUnidades
            [21-Sep-2018 23:41:55] INFO: API:   Importando tabla: SATNivelesEducativos
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoRelacion
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATMonedas
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATFormaPago
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATRegimenes
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATBancos
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATUsoCfdi
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATEstados
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATOrigenRecurso
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATPeriodicidadPago
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATRiesgoPuesto
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoContrato
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoDeduccion
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoHoras
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoIncapacidad
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoJornada
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoNomina
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoOtroPago
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoPercepcion
            [21-Sep-2018 23:41:56] INFO: API:   Importando tabla: SATTipoRegimen
            [21-Sep-2018 23:41:56] INFO: API: Importación terminada...

    * **IMPORTANTE:** Si tienes varios emisores en el sistema, **debes de migrarlos todos**.

1. Por ultimo reinicia el sistema.

        └──> sudo systemctl restart empresalibre
        [sudo] password for empresa:

    * El proceso anterior debe ser instantaneo, si se tarda más de un minuto, mira en [errores más comúnes][2].
    * También debe funcionar que reinicies la maquina virtual o servidor donde tengas el sistema.

1. Cualquier otro problema, usa el [sistema de tickets del proyecto][3]


<BR>
### Errores más comúnes

<BR>
<div class="alert-box error"><span>PRECAUCIÓN: </span>
    La modificación incorrecta de cualquier archivo del sistema, puede provocar
    que deje de funcionar. Asegurate de seguir todos los pasos correctamente.
    Si no estas seguro, es mejor contrates el soporte para que un técnico
    especializado realice el procedimiento.
</div>
<BR>

#### Al respaldar la base de datos, muestra el error

      File "/usr/lib/python3.7/site-packages/requests/adapters.py", line 513, in send
        raise ConnectionError(e, request=request)
    requests.exceptions.ConnectionError: HTTPSConnectionPool(host='seafile.empresalibre.net', port=443): Max retries exceeded with url: /api2/auth-token/ (Caused by NewConnectionError('<urllib3.connection.VerifiedHTTPSConnection object at 0x7f2f031a4550>: Failed to establish a new connection: [Errno -2] Name or service not known'))

* Entra a la carpeta

        └──> cd proyectos/empresa-libre/source/app/
        ┌─[empresalibre-][empresa]->{~/proyectos/empresa-libre/source/app}
        └──>

* Siempre saca una copia de seguridad.

        cp conf.py ~/conf.py

* Edita el archivo

        nano conf.py

* Debe de verse exactamente como el siguiente ejemplo, agrega lo que haga falta.

        #!/usr/bin/env python

        DEBUG = False
        MV = True

        #~ Establece una ruta accesible para el servidor web
        LOG_PATH = '/home/empresa/log/empresa-libre.log'

        SEAFILE_SERVER = {
            'URL': 'https://seafile.empresalibre.net/api2/',
            'USER': '',
            'PASS': '',
            'REPO': '',
        }
        SEAFILE_SERVER = {}

* Para guardar los cambios presionas: `CTRL+O`
* Para salir presionas: `CTRL+X`

<BR>
#### Al reinciar el sistema, tarda más de un minuto en terminar.

<BR>
<div class="alert-box warning"><span>CUIDADO: </span>
    Se muestra el contenido del archivo de la maquina virtual, puede ser diferente
    si lo usas en tu propio servidor o VPS.
</div>
<BR>

* Edita el archivo

        sudo nano /etc/systemd/system/empresalibre.service

* Asegurate de que este **exactamente** como en:

        [Unit]
        Description=uWSGI instance to serve Empresa Libre

        [Service]
        ExecStart=/usr/bin/uwsgi /home/empresa/empresa-libre/app/main.ini
        KillSignal=SIGQUIT

        [Install]
        WantedBy=multi-user.target

* Para guardar los cambios presionas: `CTRL+O`
* Para salir presionas: `CTRL+X`
* Recarga los cambios.

        sudo systemctl daemon-reload

* Asegurate de que ahora reinicia al instante.

        sudo systemctl restart empresalibre


[1]: #al-respaldar-la-base-de-datos-muestra-el-error
[2]: #al-reinciar-el-sistema-tarda-mas-de-un-minuto-en-terminar
[3]: https://gitlab.com/mauriciobaeza/empresa-libre/issues
