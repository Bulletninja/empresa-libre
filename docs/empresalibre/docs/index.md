## Bienvenido a la documentación de Empresa Libre

<div style="text-align: right">
<BR><B>
En cada relación comercial,<BR>
existe una relación humana.</B>
<BR>
<BR>
<BR>
</div>

**Empresa Libre** es un sistema de facturación electrónica especificamente
diseñado para la legislación mexicana. **Empresa Libre** es totalmente
[software libre][1].

### Índice

1. [Instalación y configuración](instalacion.md)
1. [Administración del sistema](administracion.md)
1. [Guía de usuario](guiadeusuario.md)
1. [Preguntas más frecuentes](preguntas.md)


### Como ayudar

* Usa **Empresa Libre**
* Reportando errores
* Escribe documentación
* Graba videos de uso
* Propon mejoras
* Conviertete en **patrocinador**



[1]: https://www.gnu.org/philosophy/free-sw.es.html
