## Bancos


<BR>

Esta herramienta nos permite llevar el control de ingresos y egresos de una o
más cuentas bancarias. También nos permite generar el CFDI (factura) de pago.

Primero, es necesario [dar de alta una cuenta bancaria][1]

![Bancos Principal](img/03/bancos_001.png)

<div class="alert-box notice"><span>TIP: </span>
    Asegurate siempre de agregar solo movimientos conciliados con tu estado de
    cuenta bancario y de, en la medida de lo posible, agregarlos en orden.
</div>

<BR>
### Fecha y saldo inicial

Estos valores estarán determinados por los que hayas capturado al [dar de alta
la cuenta bancaria][1] en el área administrativa. Mientras una cuenta no tenga
movimientos, aparte del saldo inicial, es posible eliminarla desde
administración.

![Saldo inicial](img/03/bancos_002.png)


<BR>
### Agregar depósito sin facturas relacionadas

Con el botón de comando ***+ Depósito***, agregamos un nuevo depósito. ![+](img/03/bancos_003.png)

Todos los campos con asterisco rojo son requeridos.

* **Fecha**: La fecha en que fue realizado el depósito.
* **Hora**: La hora en que fue realizado el depósito.
* **Referencia**: Referencia del depósito.
* **Forma de pago**: Selecciona de la lista la forma de pago.
* **Importe**: Captura el importe del depósito.
* **Descripción**: El detalle del concepto de este depósito.

![Agregar depósito](img/03/bancos_004.png)

Guarda con el botón de comando ***+ Guardar Depósito***. Se te preguntara para
confirmar la acción. **Asegurate que todos los datos sean correctos**.

![Confirmar](img/03/bancos_005.png)

Si todo esta bien, el nuevo movimiento será agregado a la cuenta y actualizado
el saldo de la misma.

![Nuevo depósito](img/03/bancos_006.png)

<div class="alert-box warning"><span>TIP: </span>
    La hora de los depósitos se usa al generar una Factura de Pago. El SAT, en
    caso de no saber la hora, permite usar el medío día para todos (12:00:00).
    <B>Resiste la "comodidad" de usar esto y asegurate de usar una hora diferente
    para cada movimiento.</B>
</div>


<BR>
### Agregar retiro

Con el botón de comando ***- Retiro***, agregamos un nuevo retiro.

Todos los campos con asterisco rojo son requeridos.

* **Fecha**: La fecha en que fue realizado el retiro.
* **Hora**: La hora en que fue realizado el retiro.
* **Referencia**: Referencia del retiro.
* **Forma de pago**: Selecciona de la lista la forma de pago.
* **Importe**: Captura el importe del retiro.
* **Descripción**: El detalle del concepto de este retiro.

![Agregar retiro](img/03/bancos_007.png)

Guarda con el botón de comando ***- Guardar Retiro***. Se te preguntara para
confirmar la acción. **Asegurate que todos los datos sean correctos**.

![Confirmar retiro](img/03/bancos_008.png)

Si todo esta bien, el nuevo movimiento será agregado a la cuenta y actualizado
el saldo de la misma.

![Nuevo retiro](img/03/bancos_009.png)

<div class="alert-box warning"><span>TIP: </span>
    Recuerda: <B>Resiste la "comodidad" de usar la misma hora para todos los
    movimientos.</B>
</div>


<BR>
### Pagar factura sin relacionar a un depósito

Usa el botón de comando ***+ Depósito***. En la tabla ***Facturas por pagar***,
veras las facturas pendientes de pago. Selecciona la correcta y da clic en el
botón de comando ***Solo marcar pagada***.

![Nuevo retiro](img/03/bancos_010.png)

* No captures ningún dato del formulario, solo selecciona la factura a marcar como pagada.
* **IMPORTANTE**: Solo marca facturas pagadas que **estes 100% seguro que NO requeriras generarle Factura de pago** y que no esten relacionadas con ningún depósito.

Al dar clic en el botón de comando ***Solo marcar pagada***, debes confirmar la
acción.

![Confirmar pago](img/03/bancos_011.png)

Si todo esta bien, la factura será retirada de la tabla ***Facturas por pagar***,
y recibiras una notificación de confirmación.

![Confirmar pago](img/03/bancos_012.png)

<div class="alert-box notice"><span>TIP: </span>
    Marca <B>solo como pagada</b> una factura, al no estar relacionada con
    ningún depósito, no afecta al saldo de la cuenta bancaria, pero <b>si a la
    cuenta del cliente.</b>
</div>


<BR>
### Agregar depósito relacionando facturas

Con el botón de comando ***+ Depósito***, agregamos un nuevo depósito.

Todos los campos con asterisco rojo son requeridos.

* **Fecha**: La fecha en que fue realizado el depósito.
* **Hora**: La hora en que fue realizado el depósito. **Usa diferente hora para cada movimiento**
* **Referencia**: Referencia del depósito.
* **Forma de pago**: **Es importante selecciones el valor correcto, si vas a generar Factura de Pago.**
* **Importe**: **NO captures este valor**
* **Descripción**: **NO captures este valor**

![Agregar depósito](img/03/bancos_013.png)

Para relacionar las facturas pagadas en este depósito, ubica las facturas
correctas y arrastralas a la tabla ***Facturas a pagar en este depósito***,
también puede usar un doble clic para esto. Puedes agregar una o más facturas,
conforme las vas agregando, el sistema sumara sus respectivos importes y
actualizará la descripción del depósito. La descripción puedes editarla
libremente, pero **no cambies el importe del depósito manualmente**.

![Agregar depósito](img/03/bancos_014.png)

Guarda con el botón de comando ***+ Guardar Depósito***. Se te preguntara para
confirmar la acción. **Asegurate que todos los datos sean correctos** y de que
la cantidad y las facturas relacionadas son correctas.

![Confirmar](img/03/bancos_015.png)

Si todo esta bien, el nuevo movimiento será agregado a la cuenta y actualizado
el saldo de la misma.

![Nuevo depósito](img/03/bancos_016.png)

<div class="alert-box warning"><span>REITERAMOS: </span>
    La hora de los depósitos se usa al generar una Factura de Pago. El SAT, en
    caso de no saber la hora, permite usar el medío día para todos (12:00:00).
    <B>Resiste la "comodidad" de usar esto y asegurate de usar una hora diferente
    para cada movimiento.</B>
</div>


<BR>
### Cancelar un movimiento.

En cualquier momento puedes cancelar un movimiento con el botón de comando
***Cancelar***. Es mucho mejor te asegures de capturar movimientos solo
confirmados y conciliados, en vez de estar eliminando movimientos. Cuando se
elimina un movimiento, no importa si es retiro o depósito, el saldo de la cuenta
se actualiza a partir de la fecha del movimiento eliminado y hasta el más
reciente.

* Si es un depósito y tiene facturas relacionadas, las mismas estarán de nuevo marcadas como **Por pagar** y disponibles para relacionarse con otro depósito.
* **No podrás eliminar** un depósito si este ya tiene generada una Factura de Pago. **Siempre, asegurate de que todo este correcto antes de generar una Factura de Pago**.

![Cancelar movimiento](img/03/bancos_017.png)


<BR>
### Generar Factura de Pago

Para usar esta herramienta, debe estar activado el uso del [complemento de pago][2]
dentro de administración. Si no ves en ***Bancos*** el botón de comando ***Factura
de Pago***, solicita a un administrador que lo active.

![Factura de pago](img/03/bancos_018.png)

<div class="alert-box notice"><span>TIP: </span>
    Asegurate de revisar que todos tus movimientos este correctamente capturados
    y las facturas correctas relacionadas con cada depósito, antes de usar esta
    herramienta.
</div>

<BR>

* Solo puedes generar ***Facturas de pago*** de movimientos de depósito que tengan facturas relacionadas.
* Selecciona el depósito correcto y presiona el botón de comando ***Factura de Pago***.

En la siguiente pantalla, **verifica una vez más que todos los datos con correctos**.
En este momento puedes cerrar e incluso cancelar el movimiento para hacer cualquier
corrección. Una vez generada la ***Factura de Pago***, no podrás hacer ningún
cambio. Todos los datos mostrados son de solo lectura, tanto los datos del
depósito como las facturas relacionadas para que, reiteramos, verifiques que
todos los datos son correctos.

![Factura de pago](img/03/bancos_019.png)

**En cuanto estes 100% seguro de que todos los datos son correctos**, usa el
botón de comando ***Timbrar***, para enviar a timbrar la ***Factura de Pago***.
Se te pedirá una confirmación de esta acción.

![Factura de pago](img/03/bancos_020.png)

Si todo esta bien, el sistema: guardará, generará y timbrará la nueva ***Factura
de Pago***.

![Factura de pago](img/03/bancos_021.png)

Con un clic en el icono ***PDF***, puedes generarl el PDF respectivo de esta
***Factura de Pago***. Como con las plantillas de las facturas, puedes
personalizar completamente esta plantilla.

![Factura de pago](img/03/bancos_022.png)

<BR>
### Facturas de pago con facturas en otras monedas

Cuando el sistema detecta que se están usando más de una moneda, automáticamente
te mostrará las columnas del ***Total*** y la ***Moneda*** respectiva.

![Factura de pago](img/03/bancos_023.png)

Al relacionar la factura, observa que tomará el valor original en la moneda del
documento.

![Factura de pago](img/03/bancos_024.png)

Los siguientes pasos **son importantes y en este orden**, los campos que debes de modificar en la
tabla ***Facturas a pagar en este depósito*** son:

* **Este pago**: Si el valor pagado es el total de la factura, no lo modifiques, si es parcial, captura el valor pagado **en la moneda del documento**, en este ejemplo, en UDS. Este valor se usa para el estado de cuenta de la factura.
* **Este Pago MXM**: Captura el valor pagado en moneda nacional de este documento. En este ejemplo, sería el valor **real** del depósito. Este valor se usará para la ***Factura de pago***, para el estado de cuenta y para el saldo del cliente.
* **T.C.**: Al capturar correctamente los dos valores anteriores, el sistema te calculará el tipo de cambio (***T.C***) usado, si no es correcto, puedes editarlo libremente. Este valor se usa para la ***Factura de pago***.

Ahora, nuestros datos deben verse así:

![Factura de pago](img/03/bancos_025.png)

Guarda con el botón de comando ***+ Guardar Depósito***. Se te preguntara para
confirmar la acción. **Asegurate que todos los datos sean correctos** y de que
la cantidad y las facturas relacionadas son correctas.

![Factura de pago](img/03/bancos_026.png)

Si todo esta bien, el nuevo movimiento será agregado a la cuenta y actualizado
el saldo de la misma.

Ahora si, puedes generar la ***Factura de Pago*** de este depósito.

![Factura de pago](img/03/bancos_027.png)


<BR>
### Cuenta de banco y facturas relacionadas en otras monedas

Cuando la cuenta de banco no este en M.N (MXN) y los documentos relacionados
esten en la misma moneda de la cuenta de banco, el procedimiento para generar
la factura de pago es:

Los campos que debes de modificar en la tabla ***Facturas a pagar en este depósito*** son:

* **Este pago**: Si el valor pagado es el total de la factura, no lo modifiques, si es parcial, captura el valor pagado **en la moneda del documento**, en este ejemplo, en UDS. Este valor se usa para el estado de cuenta de la factura.
* **Este Pago USD**: Captura el valor pagado en la moneda de la cuenta bancaria. En este ejemplo, sería el valor **real** del depósito. Este valor se usará para la ***Factura de pago***.
* **T.C.**: Siempre debe ser 1.00 al ser la misma moneda de la cuente bancaria y del documento.

Ahora, el tipo de cambio del movimiento, **si debes de capturarlo**, es el tipo de
cambio de la moneda de la cuenta en relación con la M.N (MXN). Este valor se usará
para la ***Factura de pago***.

![Factura de pago](img/03/bancos_028.png)

Guarda con el botón de comando ***+ Guardar Depósito***. Se te preguntara para
confirmar la acción. **Asegurate que todos los datos sean correctos** y de que
la cantidad y las facturas relacionadas son correctas.

Si todo esta bien, el nuevo movimiento será agregado a la cuenta y actualizado
el saldo de la misma.

Ahora si, puedes generar la ***Factura de Pago*** de este depósito.

![Factura de pago](img/03/bancos_029.png)


[1]: /administracion/#agregar-cuenta-bancaria
[2]: /administracion/#complementos
