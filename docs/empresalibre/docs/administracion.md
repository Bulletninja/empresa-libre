## Administración del sistema

<BR>
<div class="alert-box notice"><span>TIP: </span>
    Se requiere un usuario con derechos de <b>administración</b> para usar esta área
</div>

<BR><BR>

Para acceder al área administrativa, presiona el icono de los engranes en la
esquina superior derecha. Si no lo ves, tu usuario no tiene derechos administrativos.

![Acceso al admin](img/02/admin_001.png)

### Agregar cuenta bancaria

Para agregar una cuenta bancaria, primero hay que seleccionar el banco o bancos
a usar, esto se hace en ***Catalogos SAT***, pestaña ***Bancos***. Por default
veras primero los bancos ya seleccionados. Marca la casilla de verificación de
cada banco que deses tener disponible al agregar las cuentas bancarias. Usa la
barra de desplazamiento de la tabla o la rueda de tu ratón para ver más bancos.

![Seleccionar bancos](img/02/admin_002.png)

Ahora, desde la sección ***Emisor***, pestaña ***Cuentas de Banco***, captura
los siguiente campos, todos son requeridos:

* **Nombre**: Un nombre para identificar la cuenta, puedes usar cualquier texto
* **Banco**: Solo veras los bancos previamente seleccionados
* **Cuenta**: El número de cuenta
* **CLABE**: La clave interbancaria, se valida que este correcta.
* **Moneda**: Solo veras las monedas seleccionadas del Catalogo del SAT
* **Fecha de apertura**: La fecha de apertura de la cuenta
* **Saldo inicial**: Saldo inicial **a partir de donde se empezará a llevar el control de movimientos**
* **Fecha saldo inicial**: La fecha del saldo inicial. **No puedes capturar movimientos anteriores a esta fecha**

![Agregar cuenta bancaria](img/02/admin_003.png)

Usa el botón de comando **Agregar cuenta** para agregarla al sistema. Si todos
los campos son correctos, deberá agregarse al listado inferior.

<div class="alert-box notice"><span>TIP: </span>
    Solo puedes eliminar cuentas bancarias que no tengan movimientos capturados.
</div>

<BR>
### Complementos

Los complementos, son caracteristicas especiales dictadas por el SAT que se
pueden agregar a cada CFDI generado.

<BR>
<div class="alert-box notice"><span>TIP: </span>
    Consulta con tu departamente contable los requerimientos y caracteristicas de cada uno.
</div>

<BR>
Todos se encuentran en la sección ***Opciones***, pestaña ***Otros***. Ve a la
subsección ***Complementos*** y activa solo los que requieras.

![Agregar cuenta bancaria](img/02/admin_004.png)

* **INE**: Para CFDI emitidos a partidos politicos
* **EDU**: Solo para escuelas incorporadas a la SEP
* **Pagos**: Para generar facturas de pago.
    * La serie predeterminada es **FP**, puedes personalizar la que quieras, capturas y presionas **ENTER** para guardar el valor.
    * El folio predeterminado es **1**, puedes personalizar el que quieras, capturas y presionas **ENTER** para guardar el valor.
