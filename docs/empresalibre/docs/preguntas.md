## Preguntas más frecuentes

<div style="text-align: right">
<BR><B><FONT SIZE=+1>Software libre, no gratis</FONT></B>
<BR>
<BR>
<BR>
</div>


<BR>
### ¿Empresa Libre es gratis?

No, **Empresa Libre** no es gratis, es libre y es muy importante tener clara esta
diferencia. **Empresa Libre** es [software libre][1], lo que quiere decir que
puedes usarlo, copiarlo, mejorarlo y compartirlo con quien quieras. **Empresa
Libre** se sostiene de la venta de los folios para el timbrado y del soporte
técnico derivado de su uso, por eso, por favor, **no pidas soporte técnico gratis**.


<BR>
### Si es libre... ¿Por qué cobran el alta inicial?

Recuerda, **libre es diferente de gratis**, y el alta inicial de **Empresa Libre**,
es el costo inicial de 100 timbres fiscales, lo que estas pagando son timbres
fiscales, no el software. Nunca, reiteramos, nunca hemos cobrado el software.


<BR>
### Si el SAT dice que los PACs deben de dar timbrado gratuito... ¿Por que lo cobran Ustedes?

Efectivamente, por ley todos los PACs deben de tener una aplicación gratuita
para el timbrado. Nosotros **NO somos PAC**. Hacemos el timbrado por medio de
Finkok, el mejor PAC de México, puedes [timbrar gratuitamente en su página][2].


<BR>
### ¿Al contratar el servicio se incluye el soporte técnico?

No, no se incluye ningún tipo de soporte técnico. Dado que **Empresa Libre** es
[software libre][1] y se ofrece sin **ningún costo para todos**, el soporte técnico
es parte de los ingresos indispensables para poder seguir desarrollando y
manteniendo el sistema. Esto incluye cualquier tontería que se le ocurra al SAT.
Por eso, por favor, **no pidas soporte técnico gratis**.


<BR>
### ¿Qué alternativas tienen de soporte?

El ingreso por soporte técnico es una importante fuente de ingreso para sufragar
todas [nuestras actividades altruistas][3]. Cada vez que contratas nuestros
servicios, **estas ayudando a otros**.

1. **Soporte gratuito** en el [sistema de tickets del proyecto][4], un soporte
que hemos dado por años de forma profesional, además, otros entusiastas usuarios
del sistema, también pueden ayudarte con cualquier duda que tengas.
2. **Soporte de pago**, puedes contratar el servicio puntual por hora para un
problema en específico o contratar una póliza de soporte mensual. Ponte en
contacto con nosotros para tener más detalles.
3. Todos nuestros [donantes y patrocinadores][5] tienen todo el soporte técnico
que requieran para el sitema. Todas las [ONGs][6] que apoyamos, también tienen
todo el soporte técnico que requieran.


<BR>
### ¿Puedo probar el sistema antes de contratar el servicio?

Claro, de hecho, **es un requisito indispensable** para contratar, los datos de
acceso al sistema de pruebas son:

* **URL**: https://demo.empresalibre.net/
* **RFC**: LAN7008173R5
* **Usuario**: admin
* **Contraseña**: salgueiro3.3

Toma en cuenta que este sitio esta en constante actualización, los datos generados
se limpian de forma regular.


<BR>
### Después de actualizar, no veo los cambios esperados en pantalla

Un alto porcentaje de estos problemas, se arregla **forzando** el refresco de
tu pantalla, en la mayoría de los navegadores con la combinación de teclas
**CTRL+F5**. En raras ocasiones, es preciso limpiar el cache de tu navegador.



[1]: https://www.gnu.org/philosophy/free-sw.es.html
[2]: https://www.finkok.com/
[3]: http://universolibre.org/hacemos/
[4]: https://gitlab.com/mauriciobaeza/empresa-libre/issues
[5]:
[6]: https://universolibre.org/hacemos/
