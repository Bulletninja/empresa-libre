## Notas de lanzamiento

Siempre debe verificar en esta sección, el proceso que debes seguir con cada
actualización del sistema. Recuerda; **es muy importante mantener tu sistema
siempre actualizado.** Solo se da soporte sobre la ultima versión de **Empresa
Libre**.


### 1.19.1 [03-oct-2018]
- Error [#291](https://gitlab.com/mauriciobaeza/empresa-libre/issues/291)
- Error al generar PDF de factura de pago con relacionados sin serie
- Error al relacionar facturas versión 3.2


### 1.19.0 [28-sep-2018]
- Mejora [#280](https://gitlab.com/mauriciobaeza/empresa-libre/issues/280)
- Mejora [#288](https://gitlab.com/mauriciobaeza/empresa-libre/issues/288)
- Error [#290](https://gitlab.com/mauriciobaeza/empresa-libre/issues/290)

* IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -bk

python main.py -m
```


### 1.18.0 [27-sep-2018]
- Fix [#282](https://gitlab.com/mauriciobaeza/empresa-libre/issues/282) Factura de pago en otras monedas


### 1.17.0 [25-sep-2018]
- Fix - Al generar factura de pago con documentos relacionados en otras monedas
- Fix - Al generar factura de pago sin serie en documentos relacionados
- Fix [#278](https://gitlab.com/mauriciobaeza/empresa-libre/issues/278)
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -bk

python main.py -m
```


### 1.16.1 [18-sep-2018]
- Error [#268](https://gitlab.com/mauriciobaeza/empresa-libre/issues/268)
- IMPORTANTE: Actualizar si usas cuatro decimales en impuestos


### 1.16.0 [16-sep-2018]
- Se puede editar el saldo de un cliente
- Se muestra la cantidad de facturas de pago en los movimientos


### 1.15.0 [12-sep-2018]
- Se pueden mover las facturas con doble clic en los movimientos de banco.
- Fix - Al sumar las facturas en los depósitos
- Fix - Al importar los pedimentos en facturas por lotes
- Fix - Al guardar los datos del emisor


### 1.14.0 [10-sep-2018]
- Personalizar plantilla para factura de pago
- Fix - Mostrar serie y folio capturado para factura de pago
- Fix - Agregar nueva cuenta de banco


### 1.13.0 [10-sep-2018]
- Cancelar factura de pago
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -bk

python main.py -m
```


### 1.12.0 [31-ago-2018]
- Soporte para facturas (complemento) de pago.
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -bk

python main.py -m
```


### 1.11.1 [21-ago-2018]
- Fix - Quitar columna en tabla facturaspagos.
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```


### 1.11.0 [25-jul-2018]
- Se cambia la forma de consultar los folios restantes. Es indispensable actualizar a esta versión para ver tus timbres restantes.


### 1.10.0 [10-jul-2018]
- Ahora se pueden manejar precios con cuatro decimales.


### 1.9.3 [08-jul-2018]
- Fix: Al refacturar conceptos con descuento


### 1.9.2 [05-jul-2018]
- Fix: Al generar el reporte de facturas en PDF


### 1.9.1 [25-jun-2018]
- Fix: Al mostrar el título de la aplicación    - Se agrega el registro de acción al borrar una factura


### 1.9.0 [18-jun-2018]
- Se agrega la vista del detalle de facturas
- Fix: Al timbrar nómina


### 1.8.1 [14-jun-2018]
- Fix: Se agrega una barra de desplazamiento al buscar productos o clientes
- Se cambia el servidor de consulta de timbres


### 1.8.0 [03-jun-2018]
- Se permiten 4 decimales en Tipo de cambio
- Se agrega el campo {total_cantidades} al generar el PDF
- Se agrega opción para generar respaldos de la BD en MV
- Fix: Al generar con complemento EDU


### 1.7.0 [23-may-2018]
- Se agrega soporte para truncar impuestos locales, para las estulticias de los "ingenieros" de las dependencias de gobierno


### 1.6.1 [09-abr-2018]
- Fix: Nómina con separación


### 1.6.0 [18-feb-2018]
- Facturacion a extranjeros
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```


### 1.5.0 [30-ene-2018]
- Timbrado de Nómina
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```


### 1.4.0 [01-ene-2018]
- Impresión de tickets


### 1.3.0 [27-Dic-2017]
- Punto de venta
- IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```


### 1.2.0 [18-Dic-2017]
- IMPORTANTE: Es necesario actualizar la base de datos, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -bd
```


### 0.1.0 [26-Oct-2017]
- Generar y timbrar con CFDI 3.3

