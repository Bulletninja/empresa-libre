#!/usr/bin/env python3

import falcon
from falcon_multipart.middleware import MultipartMiddleware
from beaker.middleware import SessionMiddleware

from middleware import (
    AuthMiddleware,
    JSONTranslator,
    ConnectionMiddleware,
    static,
    handle_404
)
from models.db import StorageEngine
from controllers.main import (AppEmpresas,
    AppLogin, AppLogout, AppAdmin, AppEmisor, AppConfig,
    AppMain, AppValues, AppPartners, AppProducts, AppInvoices, AppFolios,
    AppDocumentos, AppFiles, AppPreInvoices, AppCuentasBanco,
    AppMovimientosBanco, AppTickets, AppStudents, AppEmployees, AppNomina,
    AppInvoicePay, AppCfdiPay
)


from settings import DEBUG, MV, NO_HTTPS, PATH_SESSIONS


db = StorageEngine()

api = falcon.API(middleware=[
    AuthMiddleware(),
    JSONTranslator(),
    ConnectionMiddleware(),
    MultipartMiddleware(),
])
api.req_options.auto_parse_form_urlencoded = True
api.add_sink(handle_404, '')

api.add_route('/empresas', AppEmpresas(db))
api.add_route('/', AppLogin(db))
api.add_route('/logout', AppLogout(db))
api.add_route('/admin', AppAdmin(db))
api.add_route('/emisor', AppEmisor(db))
api.add_route('/folios', AppFolios(db))
api.add_route('/main', AppMain(db))
api.add_route('/values/{table}', AppValues(db))
api.add_route('/files/{table}', AppFiles(db))
api.add_route('/config', AppConfig(db))
api.add_route('/doc/{type_doc}/{id_doc}', AppDocumentos(db))
api.add_route('/partners', AppPartners(db))
api.add_route('/products', AppProducts(db))
api.add_route('/invoices', AppInvoices(db))
api.add_route('/preinvoices', AppPreInvoices(db))
api.add_route('/tickets', AppTickets(db))
api.add_route('/cuentasbanco', AppCuentasBanco(db))
api.add_route('/movbanco', AppMovimientosBanco(db))
api.add_route('/students', AppStudents(db))
api.add_route('/employees', AppEmployees(db))
api.add_route('/nomina', AppNomina(db))
api.add_route('/invoicepay', AppInvoicePay(db))
api.add_route('/cfdipay', AppCfdiPay(db))


# ~ Activa si usas waitress y NO estas usando servidor web
# ~ api.add_sink(static, '/static')

session_options = {
    'session.type': 'file',
    'session.cookie_expires': True,
    'session.httponly': True,
    'session.secure': True,
    'session.data_dir': PATH_SESSIONS['data'],
    'session.lock_dir': PATH_SESSIONS['lock'],
}
# ~ Si no usas (NO deberías) certificados en tu servidor, ponla siempre en False
if DEBUG or MV or NO_HTTPS:
    session_options['session.secure'] = False

app = SessionMiddleware(api, session_options)

