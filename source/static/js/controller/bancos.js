//~ Empresa Libre
//~ Copyright (C) 2016-2018  Mauricio Baeza Servin (web@correolibre.net)
//~
//~ This program is free software: you can redistribute it and/or modify
//~ it under the terms of the GNU General Public License as published by
//~ the Free Software Foundation, either version 3 of the License, or
//~ (at your option) any later version.
//~
//~ This program is distributed in the hope that it will be useful,
//~ but WITHOUT ANY WARRANTY; without even the implied warranty of
//~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//~ GNU General Public License for more details.
//~
//~ You should have received a copy of the GNU General Public License
//~ along with this program.  If not, see <http://www.gnu.org/licenses/>.


var msg = ''
var msg_importe = ''
var current_currency = ''
var current_way_payment = ''


function init_config_bank(){
    var multi_currency = get_config('multi_currency')
    var g1 = $$('grid_cfdi_por_pagar')
    var g2 = $$('grid_cfdi_este_deposito')
    var g3 = $$('grid_bank_invoice_pay')

    if(multi_currency){
        g1.showColumn('total')
        g1.showColumn('currency')
        g2.showColumn('total')
        g2.showColumn('currency')
        g2.showColumn('this_pay')
        g2.showColumn('type_change')
        g3.showColumn('total')
        g3.showColumn('currency')
    }
    show('cmd_complemento_pago', get_config('used_cfdi_pays'))
    show('cmd_show_invoice_pay', get_config('used_cfdi_pays'))
    set_year_month()
}


var bancos_controllers = {
    init: function(){
        $$('lst_cuentas_banco').attachEvent('onChange', lst_cuentas_banco_change)
        $$('cmd_agregar_retiro').attachEvent('onItemClick', cmd_agregar_retiro_click)
        $$('cmd_agregar_deposito').attachEvent('onItemClick', cmd_agregar_deposito_click)
        $$('cmd_complemento_pago').attachEvent('onItemClick', cmd_complemento_pago_click)
        $$('cmd_guardar_retiro').attachEvent('onItemClick', cmd_guardar_retiro_click)
        $$('cmd_guardar_deposito').attachEvent('onItemClick', cmd_guardar_deposito_click)
        $$('cmd_cancelar_movimiento').attachEvent('onItemClick', cmd_cancelar_movimiento_click)
        $$('cmd_invoice_payed').attachEvent('onItemClick', cmd_invoice_payed_click)
        $$('txt_retiro_importe').attachEvent('onChange', txt_retiro_importe_change)
        $$('txt_deposito_importe').attachEvent('onChange', txt_deposito_importe_change)
        $$('deposit_type_change').attachEvent('onChange', deposit_type_change_change)
        $$('grid_cfdi_este_deposito').attachEvent('onAfterDrop', grid_cfdi_este_deposito_after_drop)
        $$('grid_cfdi_por_pagar').attachEvent('onAfterDrop', grid_cfdi_por_pagar_after_drop)
        $$('grid_cfdi_este_deposito').attachEvent('onBeforeEditStop', grid_cfdi_este_deposito_before_edit_stop)
        $$('grid_cfdi_este_deposito').attachEvent('onAfterEditStop', grid_cfdi_este_deposito_after_edit_stop)
        $$('filter_cuenta_year').attachEvent('onChange', filter_cuenta_change)
        $$('filter_cuenta_month').attachEvent('onChange', filter_cuenta_change)
        $$('filter_cuenta_dates').attachEvent('onChange', filter_cuenta_dates_change)

        $$('cmd_pay_stamp').attachEvent('onItemClick', cmd_pay_stamp_click)
        $$('cmd_pay_cancel').attachEvent('onItemClick', cmd_pay_cancel_click)
        $$('cmd_pay_delete').attachEvent('onItemClick', cmd_pay_delete_click)
        $$('grid_cfdi_pay').attachEvent('onItemClick', grid_cfdi_pay_click)
        $$('grid_cfdi_por_pagar').attachEvent('onItemDblClick', grid_cfdi_por_pagar_double_click)
        $$('grid_cfdi_este_deposito').attachEvent('onItemDblClick', grid_cfdi_este_deposito_double_click)
        $$('cmd_show_invoice_pay').attachEvent('onItemClick', cmd_show_invoice_pay_click)
        $$('filter_invoice_pay_year').attachEvent('onChange', filter_invoice_pay_change)
        $$('filter_invoice_pay_month').attachEvent('onChange', filter_invoice_pay_change)
        $$('grid_bank_invoice_pay').attachEvent('onItemClick', grid_bank_invoice_pay_click)

        init_config_bank()
    }
}


function set_year_month(){
    var d = new Date()
    var y =  $$('filter_cuenta_year')
    var y2 =  $$('filter_invoice_pay_year')
    var m =  $$('filter_cuenta_month')

    webix.ajax().get('/values/cuentayears', {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            y.getList().parse(values)
            y2.getList().parse(values)
            y.blockEvent()
            m.blockEvent()
            y.setValue(d.getFullYear())
            m.setValue(d.getMonth() + 1)
            y.unblockEvent()
            m.unblockEvent()
        }
    })
}


function get_cuentas_banco(){
    var list =  $$('lst_cuentas_banco')

    webix.ajax().get('/cuentasbanco', {'tipo': 1}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                list.getList().parse(values.rows)
                list.blockEvent()
                list.setValue(values.rows[0].id)
                list.unblockEvent()
                $$('txt_cuenta_moneda').setValue(values.moneda)
                $$('txt_cuenta_saldo').setValue(values.saldo)
                get_estado_cuenta()
                current_currency = values.key_currency
                //~ set_is_mn()
            }else{
                enable('cmd_agregar_retiro', false)
                enable('cmd_agregar_deposito', false)
            }
        }
    })
}


function get_estado_cuenta(rango){
    if(rango == undefined){
        var filtro = {
            cuenta: $$('lst_cuentas_banco').getValue(),
            year: $$('filter_cuenta_year').getValue(),
            mes: $$('filter_cuenta_month').getValue(),
        }
    }else{
        var filtro = {
            cuenta: $$('lst_cuentas_banco').getValue(),
            fechas: rango,
        }
    }

    var grid = $$('grid_cuentabanco')

    webix.ajax().get('/movbanco', filtro, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            grid.clearAll()
            if (values.ok){
                grid.parse(values.rows, 'json')
            }
        }
    })
}


function get_saldo_cuenta(){
    var id = $$('lst_cuentas_banco').getValue()
    webix.ajax().get('/values/saldocuenta', {id: id}, function(text, data){
        var value = data.json()
        if(value){
            $$('txt_cuenta_saldo').setValue(value)
        }else{
            msg = 'No se pudo consultar el saldo'
            msg_error(msg)
        }
    })
}


function get_account_currency(){
    var id = $$('lst_cuentas_banco').getValue()
    webix.ajax().get('/cuentasbanco', {'id': id, 'opt': 'currency'}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('txt_cuenta_moneda').setValue(values.currency)
                current_currency = values.key_currency
            }
        }
    })
}


function lst_cuentas_banco_change(nv, ov){
    get_saldo_cuenta()
    get_estado_cuenta()
    get_account_currency()
}


function get_bancos_forma_pago(retiro){
    var values = table_waypayment.chain().find({'id': { '$ne' : '99' }}).data()
    if(retiro){
        lst = $$('lst_retiro_forma_pago')
    }else{
        lst = $$('lst_deposito_forma_pago')
    }
    lst.getList().parse(values)
    if(current_way_payment){
        lst.setValue(current_way_payment)
    }
}


function get_facturas_por_pagar(){
    var grid1 = $$('grid_cfdi_este_deposito')
    var grid2 = $$('grid_cfdi_por_pagar')

    var ids = []
    grid1.data.each(function(obj){
        ids.push(obj.id)
    })

    webix.ajax().get('/invoices', {'opt': 'porpagar', 'ids': ids}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            grid2.clearAll()
            if (values.ok){
                grid2.parse(values.rows, 'json')
            }
        }
    })
}


function cmd_agregar_retiro_click(){
    get_bancos_forma_pago(true)
    var title = 'Agregar retiro de banco a la cuenta ' + $$('lst_cuentas_banco').getText() + ' en ' + $$('txt_cuenta_moneda').getValue()
    $$('title_bank_retiro').setValue(title)
    $$('multi_bancos').setValue('banco_retiro')
}


function cmd_agregar_deposito_click(){
    msg_importe = ''
    get_bancos_forma_pago(false)
    get_facturas_por_pagar()

    var g = $$('grid_cfdi_este_deposito')
    g.config.columns[g.getColumnIndex('importe')].header = 'Este Pago ' + current_currency
    g.refreshColumns()
    show('deposit_type_change', current_currency!=CURRENCY_MN)

    var title = 'Agregar depósito de banco a la cuenta ' + $$('lst_cuentas_banco').getText() + ' en ' + $$('txt_cuenta_moneda').getValue()
    $$('title_bank_deposit').setValue(title)

    $$('multi_bancos').setValue('banco_deposito')
}


function validate_retiro(values){
    var importe = values.retiro_importe.replace('$', '').replace(',', '').trim()

    if(!importe){
        msg = 'El importe es requerido'
        msg_error(msg)
        return false
    }
    importe = parseFloat(importe).round(2)
    if(importe <= 0){
        msg = 'El importe debe ser mayor a cero'
        msg_error(msg)
        return false
    }

    if(!values.retiro_descripcion.trim()){
        msg = 'La descripción es requerida'
        msg_error(msg)
        return false
    }

    var today = new Date()
    if(values.retiro_fecha > today){
        msg = 'Fecha inválida, es una fecha futura'
        msg_error(msg)
        return
    }

    var horas = $$('time_retiro').getText().split(':')
    var seg = parseInt(horas[2])
    var min = parseInt(horas[1])
    var horas = parseInt(horas[0])

    if(horas > 23){
        focus('time_retiro')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(min > 59){
        focus('time_retiro')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(seg > 59){
        focus('time_retiro')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }

    return true
}


function guardar_retiro(values){
    var form = $$('form_banco_retiro')

    var importe = get_float(values.retiro_importe)
    var data = new Object()
    data['opt'] = 'add'
    data['cuenta'] = $$('lst_cuentas_banco').getValue()
    data['fecha'] = values.retiro_fecha
    data['hora'] = $$('time_retiro').getText()
    data['numero_operacion'] = values.retiro_referencia.trim()
    data['forma_pago'] = $$('lst_retiro_forma_pago').getValue()
    data['retiro'] = importe
    data['deposito'] = 0.0
    data['descripcion'] = values.retiro_descripcion

    webix.ajax().post('/movbanco', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('txt_cuenta_saldo').setValue(values.saldo)
                get_estado_cuenta()
                $$('multi_bancos').setValue('banco_home')
                form.setValues({})
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_guardar_retiro_click(){
    var form = $$('form_banco_retiro')

    if(!form.validate()) {
        msg_error('Valores inválidos')
        return
    }

    var values = form.getValues()
    if(!validate_retiro(values)){
        return
    }

    msg = 'Todos los datos son correctos.<br><br>¿Deseas agregar este retiro?'
    webix.confirm({
        title: 'Guardar Retiro',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                guardar_retiro(values)
            }
        }
    })
}


function txt_retiro_importe_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}

function txt_deposito_importe_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}


function deposit_type_change_change(new_value, old_value){
    if(!isFinite(new_value) || !new_value){
        this.config.value = old_value
        this.refresh()
    }
}

function get_type_change(){

}


function actualizar_deposito(grid){
    grid.sort("#fecha#", "desc", "date")

    var suma = 0
    var descripcion = ''

    grid.data.each(function(obj){
        descripcion += 'Pago de la factura: ' + obj.serie + obj.folio + ' del '
        descripcion += 'cliente: ' + obj.cliente + '\n'
        if(obj.importe == undefined){
            obj.importe = obj.saldo
            obj.this_pay = obj.saldo
        }
        suma += obj.importe.to_float()
        if(obj.type_change == undefined){
            if(obj.currency==CURRENCY_MN || obj.currency==current_currency){
                obj.type_change = 1.00
            }
        }
    })

    $$('txt_deposito_importe').setValue(suma.round(DECIMALES))
    $$('deposito_descripcion').setValue(descripcion.slice(0, -1))
    grid.refresh()
}


function grid_cfdi_por_pagar_after_drop(context, native_event){
    var grid = $$('grid_cfdi_este_deposito')
    actualizar_deposito(grid)
}


function grid_cfdi_este_deposito_after_drop(context, native_event){
    var grid = $$('grid_cfdi_este_deposito')
    actualizar_deposito(grid)
}


function grid_cfdi_este_deposito_after_edit_stop(state, editor, ignoreUpdate){
    var grid = $$('grid_cfdi_este_deposito')

    var suma = 0
    grid.data.each(function(obj){
        suma += obj.importe.to_float()
    })
    $$('txt_deposito_importe').setValue(suma)
}


function grid_cfdi_este_deposito_before_edit_stop(state, editor){
    var grid = $$('grid_cfdi_este_deposito')
    var row = grid.getItem(editor.row)

    if(editor.column == 'importe'){
        var importe = parseFloat(state.value)
        if(isNaN(importe)){
            msg = 'El importe a pagar debe ser un número'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
        if(importe <= 0){
            msg = 'El importe a pagar debe ser mayor a cero'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
        var saldo = row['saldo'].to_float()
        if(row['currency']==CURRENCY_MN && importe > saldo){
            msg = 'El importe a pagar no puede ser mayor al saldo de la factura'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }

        var this_pay = row['this_pay'].to_float()
        row['type_change'] = (importe / this_pay).round(DECIMALES_TAX)
    }

    if(editor.column == 'this_pay'){
        var importe = parseFloat(state.value)
        if(isNaN(importe)){
            msg = 'El importe a pagar debe ser un número'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
        if(importe <= 0){
            msg = 'El importe a pagar debe ser mayor a cero'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
        var saldo = row['saldo'].to_float()
        if(importe > saldo){
            msg = 'El importe a pagar no puede ser mayor al saldo de la factura'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
    }



}


function validate_deposito(values){
    var grid = $$('grid_cfdi_este_deposito')
    var importe = values.deposito_importe.to_float()
    var type_change = values.deposit_type_change.to_float4()
    var msg_tc = ''

    if(!importe){
        msg = 'El importe es requerido'
        msg_error(msg)
        return false
    }

    if(importe <= 0){
        msg = 'El importe debe ser mayor a cero'
        msg_error(msg)
        return false
    }

    if(!values.deposito_descripcion.trim()){
        msg = 'La descripción es requerida'
        msg_error(msg)
        return false
    }

    if(grid.count() && current_currency!=CURRENCY_MN){
        if(type_change <= 1.0){
            msg = 'El Tipo de Cambio debe ser mayor a 1.00'
            msg_error(msg)
            return false
        }
    }

    var today = new Date()
    if(values.deposito_fecha > today){
        msg = 'Fecha inválida, es una fecha futura'
        msg_error(msg)
        return
    }

    var horas = $$('time_deposito').getText().split(':')
    var seg = parseInt(horas[2])
    var min = parseInt(horas[1])
    var horas = parseInt(horas[0])

    if(horas > 23){
        focus('time_deposito')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(min > 59){
        focus('time_deposito')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(seg > 59){
        focus('time_deposito')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }

    if(grid.count()){
        var suma = 0
        grid.data.each(function(obj){
            var tmp = obj.importe.to_float()
            if(tmp <= 0){
                msg = 'El importe de la factura: ' + obj.serie + obj.folio + ' no puede ser menor a cero'
                msg_error(msg)
                return false
            }
            suma += tmp

            if(obj.currency!=CURRENCY_MN && obj.currency!=current_currency){
                if(obj.type_change==undefined){
                    msg_tc = 'Captura el Tipo de Cambio'
                }else{
                    var tc = obj.type_change
                    if(tc <= 1.0){
                        msg_tc = 'El Tipo de Cambio debe ser mayor a 1.00'
                    }
                }
            }
        })
        suma = suma.round(DECIMALES)

        if(msg_tc){
            msg_error(msg_tc)
            return false
        }

        if(suma > importe){
            msg = 'La suma del pago de facturas, no puede ser mayor al deposito'
            msg_error(msg)
            return false
        }

        if(suma < importe){
            msg_importe = 'El importe del depósito en mayor a la suma de facturas. '
            msg_importe += 'Asegurate de que esto sea correcto'
        }
    }

    return true
}


function guardar_deposito(values){
    var form = $$('form_banco_deposito')
    var grid = $$('grid_cfdi_este_deposito')

    var data = new Object()
    data['opt'] = 'add'
    data['cuenta'] = $$('lst_cuentas_banco').getValue()
    data['fecha'] = values.deposito_fecha
    data['hora'] = $$('time_deposito').getText()
    data['numero_operacion'] = values.deposito_referencia.trim()
    data['forma_pago'] = $$('lst_deposito_forma_pago').getValue()
    data['deposito'] = values.deposito_importe.to_float()
    data['tipo_cambio'] = values.deposit_type_change.to_float4()
    data['retiro'] = 0.0
    data['descripcion'] = values.deposito_descripcion

    current_way_payment = data['forma_pago']

    if(grid.count()){
        var ids = new Object()
        grid.data.each(function(obj){
            ids[obj.id] = {
                'this_pay': obj.this_pay.to_float(),
                'importe': obj.importe.to_float(),
                'type_change': obj.type_change,
            }
        })
        data['ids'] = ids
    }

    webix.ajax().post('/movbanco', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('txt_cuenta_saldo').setValue(values.saldo)
                get_estado_cuenta()
                $$('multi_bancos').setValue('banco_home')
                form.setValues({})
                grid.clearAll()
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_guardar_deposito_click(){
    var form = $$('form_banco_deposito')
    var grid = $$('grid_cfdi_este_deposito')

    if(!form.validate()) {
        msg_error('Valores inválidos')
        return
    }

    var values = form.getValues()
    if(!validate_deposito(values)){
        return
    }

    if(!grid.count()){
        msg = 'Todos los datos son correctos<br>br>'
        msg = 'El depósito no tiene facturas relacionadas<br><br>¿Estás '
        msg += ' seguro de guardar el depósito sin facturas relacionadas?'
        webix.confirm({
            title: 'Guardar depósito',
            ok: 'Si',
            cancel: 'No',
            type: 'confirm-error',
            text: msg,
            callback:function(result){
                if(result){
                    guardar_deposito(values)
                }
            }
        })
    }else{
        if(!msg_importe){
            msg_importe = 'Se van a relacionar ' + grid.count() + ' facturas.'
        }
        msg = 'Todos los datos son correctos.<br><br>' + msg_importe + '<br><br>'
        msg += '¿Deseas agregar este depósito?'
        webix.confirm({
            title: 'Guardar depósito',
            ok: 'Si',
            cancel: 'No',
            type: 'confirm-error',
            text: msg,
            callback:function(result){
                if(result){
                    guardar_deposito(values)
                }
            }
        })
    }
}


function cancelar_movimiento(id){
    var data = {'opt': 'cancel', 'id': id}
    webix.ajax().post('/movbanco', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                get_estado_cuenta()
                $$('txt_cuenta_saldo').setValue(values.balance)
                msg_ok(values.msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function cmd_cancelar_movimiento_click(){
    var grid = $$('grid_cuentabanco')

    var row = grid.getSelectedItem()
    if(row == undefined){
        msg_error('Selecciona un movimiento')
        return
    }
    if(row.descripcion == 'Saldo inicial'){
        msg_error('No es posible eliminar el saldo inicial')
        return
    }

    var msg = '¿Estás seguro de cancelar el movimiento seleccionado?<BR><BR>'
    msg += 'SI EL MOVIMIENTO TIENE FACTURA DE PAGO, QUEDARÁ HUERFANA<BR><BR>'
    msg += 'ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>'
    webix.confirm({
        title: 'Cancelar Movimiento',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback: function(result){
            if (result){
                cancelar_movimiento(row['id'])
            }
        }
    })
}


function set_invoices_payed(rows){
    var ids = []

    for(var row of rows){
        ids.push(row.id)
    }

    webix.ajax().post('/invoices', {opt: 'invoicepayed', ids: ids}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                msg = 'Facturas marcadas pagadas correctamente'
                $$('grid_cfdi_por_pagar').remove(values.rows)
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_invoice_payed_click(){
    var rows = $$('grid_cfdi_por_pagar').getSelectedItem(true)
    if (rows.length == 0){
        msg_error('Selecciona al menos una factura por pagar')
        return
    }

    msg = '¿Estás seguro de marcar como pagadas las facturas seleccionadas?<BR><BR>'
    msg += 'No se relacionará con ningún depósito y no podrás generarle Factura de pago.<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER'
    webix.confirm({
        title: 'Facturas Pagadas',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback: function(result){
            if (result){
                set_invoices_payed(rows)
            }
        }
    })

}


function filter_cuenta_change(){
    get_estado_cuenta()
}


function filter_invoice_pay_change(){
    get_invoices_pay()
}


function filter_cuenta_dates_change(range){
    if(range.start != null && range.end != null){
        get_estado_cuenta(range)
    }
}


function set_data_pay(row){
    var form = $$('form_bank_pay')
    var dt = row.fecha.split(' ')
    var grid = $$('grid_pay_related')
    grid.clearAll()

    set_way_payment('pay_way_payment')
    var wp = table_waypayment.findOne({'value': row.way_payment})

    form.setValues({
        id_mov: row.id,
        pay_date: dt[0],
        pay_time: dt[1],
        pay_reference: row.numero_operacion,
        pay_way_payment: wp.id,
        pay_import: row.deposito,
        pay_description: row.descripcion
    })

    webix.ajax().get('/invoicepay', {'opt': 'related', 'id': row.id}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                grid.parse(values.rows, 'json')
            }
        }
    })

    $$('grid_cfdi_pay').clearAll()
    webix.ajax().get('/cfdipay', {'opt': 'related', 'id_mov': row.id}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('grid_cfdi_pay').parse(values.rows, 'json')
            }
        }
    })

}


function cmd_complemento_pago_click(){
    var grid = $$('grid_cuentabanco')

    var row = grid.getSelectedItem()
    if(row == undefined){
        msg_error('Selecciona un movimiento de depósito')
        return
    }
    if(row.descripcion == 'Saldo inicial'){
        msg_error('No es posible generar un pago del Saldo Inicial')
        return
    }
    if(row.deposito == 0){
        msg_error('Selecciona un movimiento de depósito')
        return
    }

    set_data_pay(row)
    $$('multi_bancos').setValue('bank_pay')
}


function validate_cfdi_pay(form){
    if(!form.validate()) {
        msg_error('Valores inválidos')
        return false
    }

    var grid = $$('grid_pay_related')
    if(grid.count() == 0){
        msg_error('El depósito no tiene facturas relacionadas')
        return false
    }

    return true
}


function update_grid_cfdi_pay(row){
    var g = $$('grid_cfdi_pay')

    g.add(result.row)
    if (g.count() == 1){
        g.adjustColumn('index')
        g.adjustColumn('serie')
        g.adjustColumn('folio')
        g.adjustColumn('fecha')
        g.adjustColumn('cliente')
        g.adjustColumn('xml')
        g.adjustColumn('pdf')
        g.adjustColumn('email')
    }
}

function send_stamp_cfdi_pay(id_mov){
    var g = $$('grid_cfdi_pay')
    var data = {'opt': 'stamp', 'id_mov': id_mov}

    //~ ToDo Actualizar cantidad de facturas de pago en el movimiento

    webix.ajax().sync().post('cfdipay', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            result = data.json();
            if(result.ok){
                g.updateItem(result.id, result.row)
                msg_ok(result.msg)
            }else{
                msg_error(result.msg)
            }
        }
    })
}

function save_cfdi_pay(form){
    var values = form.getValues()
    var data = {'opt': 'new', 'id_mov': values.id_mov}

    webix.ajax().sync().post('cfdipay', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            result = data.json();
            if(result.ok){
                if(result.new){
                    msg_ok('Factura guardada correctamente<BR>Enviando a timbrar...')
                    update_grid_cfdi_pay(result.row)
                }else{
                    msg_ok('Enviando a timbrar...')
                }
                send_stamp_cfdi_pay(values.id_mov)
            }else{
                msg_error(result.msg)
            }
        }
    })
}


function cmd_pay_stamp_click(){
    var form = $$('form_bank_pay')
    var title = 'Timbrar Factura de Pago'
    msg = '¿Estás seguro de enviar a timbrar este pago?<BR><BR>EL MOVIMIENTO YA NO PODRÁ SER MODIFICADO'

    if (!validate_cfdi_pay(form)){
        return
    }

    webix.confirm({
        title: title,
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                save_cfdi_pay(form)
            }
        }
    })
}


function cmd_pay_cancel_click(){
    var form = $$('form_bank_pay')
    var values = form.getValues()
    var data = {'opt': 'cancel', 'id_mov': values.id_mov}

    var grid = $$('grid_cfdi_pay')
    if(grid.count() == 0){
        msg_error('El depósito no tiene facturas de pago activas')
        return
    }

    msg = '¿Estás seguro de cancelar esta factura?\n\nESTA ACCIÓN NO SE PUEDE DESHACER'
    webix.confirm({
        title: 'Cancelar Factura',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                webix.ajax().post('/cfdipay', data, {
                    error:function(text, data, XmlHttpRequest){
                        msg = 'Ocurrio un error, consulta a soporte técnico'
                        msg_error(msg)
                    },
                    success:function(text, data, XmlHttpRequest){
                        values = data.json();
                        if(values.ok){
                            grid.updateItem(values.id, {'estatus': 'Cancelada'})
                            msg_ok(values.msg)
                        }else{
                            msg_error(values.msg)
                        }
                    }
                })
            }
        }
    })

}


function cmd_pay_delete_click(){
    var form = $$('form_bank_pay')
    var values = form.getValues()
    var data = {'opt': 'delete', 'id_mov': values.id_mov}

    var grid = $$('grid_cfdi_pay')
    if(grid.count() == 0){
        msg_error('El depósito no tiene facturas de pago')
        return
    }

    msg = '¿Estás seguro de eliminar esta factura?<BR><BR>ASEGURATE QUE NO ESTE TIMBRADA<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER'
    webix.confirm({
        title: 'Eliminar Factura',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                webix.ajax().post('/cfdipay', data, {
                    error:function(text, data, XmlHttpRequest){
                        msg = 'Ocurrio un error, consulta a soporte técnico'
                        msg_error(msg)
                    },
                    success:function(text, data, XmlHttpRequest){
                        values = data.json();
                        if(values.ok){
                            grid.remove(grid.getFirstId())
                            msg_ok(values.msg)
                        }else{
                            msg_error(values.msg)
                        }
                    }
                })
            }
        }
    })

}


function send_cfdi_email(row){
    if(!row.uuid){
        msg_error('La factura no esta timbrada')
        return
    }

    msg = '¿Estás seguro de enviar por correo esta factura?'
    webix.confirm({
        title: 'Enviar Factura',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                webix.ajax().post('/cfdipay', {'opt': 'send', 'id': row.id}, {
                    error:function(text, data, XmlHttpRequest){
                        msg = 'Ocurrio un error, consulta a soporte técnico'
                        msg_error(msg)
                    },
                    success:function(text, data, XmlHttpRequest){
                        values = data.json();
                        if(values.ok){
                            msg_ok(values.msg)
                        }else{
                            msg_error(values.msg)
                        }
                    }
                })
            }
        }
    })
}


function grid_cfdi_pay_click(id, e, node){
    var row = this.getItem(id)

    if(id.column == 'xml'){
        location = '/doc/xmlpago/' + row.id
    }else if(id.column == 'pdf'){
        window.open('/doc/pdfpago/' + row.id, '_blank')
    }else if(id.column == 'email'){
        send_cfdi_email(row)
    }

}


function grid_bank_invoice_pay_click(id, e, node){
    var row = this.getItem(id)

    if(id.column == 'xml'){
        location = '/doc/xmlpago/' + row.id
    }else if(id.column == 'pdf'){
        window.open('/doc/pdfpago/' + row.id, '_blank')
    }else if(id.column == 'email'){
        send_cfdi_email(row)
    }

}


function grid_cfdi_por_pagar_double_click(id, e, node){
    var grid = $$('grid_cfdi_este_deposito')

    this.move(id.row, -1, grid)
    actualizar_deposito(grid)
}


function grid_cfdi_este_deposito_double_click(id, e, node){
    var grid = $$('grid_cfdi_este_deposito')

    this.move(id.row, -1, $$('grid_cfdi_por_pagar'))
    actualizar_deposito(grid)
}


function cmd_show_invoice_pay_click(){
    var y = $$('filter_invoice_pay_year')
    var m = $$('filter_invoice_pay_month')
    y.blockEvent()
    m.blockEvent()
    y.setValue($$('filter_cuenta_year').getValue())
    m.setValue($$('filter_cuenta_month').getValue())
    y.unblockEvent()
    m.unblockEvent()
    get_invoices_pay()
    $$('multi_bancos').setValue('bank_invoice_pay')
}


function get_invoices_pay(rango){
    if(rango == undefined){
        var filtro = {
            opt: 'table',
            year: $$('filter_invoice_pay_year').getValue(),
            month: $$('filter_invoice_pay_month').getValue(),
        }
    }else{
        var filtro = {
            opt: 'table',
            fechas: rango,
        }
    }

    var grid = $$('grid_bank_invoice_pay')

    webix.ajax().get('/cfdipay', filtro, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            grid.clearAll()
            if (values.ok){
                grid.parse(values.rows, 'json')
            }
        }
    })
}
