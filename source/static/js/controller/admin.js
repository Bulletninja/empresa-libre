//~ Empresa Libre
//~ Copyright (C) 2016-2018  Mauricio Baeza Servin (web@correolibre.net)
//~
//~ This program is free software: you can redistribute it and/or modify
//~ it under the terms of the GNU General Public License as published by
//~ the Free Software Foundation, either version 3 of the License, or
//~ (at your option) any later version.
//~
//~ This program is distributed in the hope that it will be useful,
//~ but WITHOUT ANY WARRANTY; without even the implied warranty of
//~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//~ GNU General Public License for more details.
//~
//~ You should have received a copy of the GNU General Public License
//~ along with this program.  If not, see <http://www.gnu.org/licenses/>.


var msg = ''
var tb_options = null
var tb_sat = null


var controllers = {
    init: function(){
        //~ Admin
        $$('menu_user').attachEvent('onMenuItemClick', menu_user_click)
        $$('multi_admin').attachEvent('onViewChange', multi_admin_change)
        //~ Emisor
        $$('cmd_save_emisor').attachEvent('onItemClick', cmd_save_emisor_click)
        $$('emisor_cp').attachEvent('onKeyPress', emisor_postal_code_key_press)
        $$('emisor_cp').attachEvent('onTimedKeyPress', emisor_postal_code_key_up)
        $$('chk_escuela').attachEvent('onChange', chk_escuela_change)
        $$('chk_ong').attachEvent('onChange', chk_ong_change)
        $$('cmd_subir_certificado').attachEvent('onItemClick', cmd_subir_certificado_click)
        $$('up_cert').attachEvent('onUploadComplete', up_cert_upload_complete)
        $$('cmd_agregar_serie').attachEvent('onItemClick', cmd_agregar_serie_click)
        $$('grid_folios').attachEvent('onItemClick', grid_folios_click)
        $$('cmd_probar_correo').attachEvent('onItemClick', cmd_probar_correo_click)
        $$('cmd_guardar_correo').attachEvent('onItemClick', cmd_guardar_correo_click)
        $$('emisor_logo').attachEvent('onItemClick', emisor_logo_click)
        $$('cmd_emisor_agregar_cuenta').attachEvent('onItemClick', cmd_emisor_agregar_cuenta_click)
        $$('cmd_emisor_eliminar_cuenta').attachEvent('onItemClick', cmd_emisor_eliminar_cuenta_click)
        $$('cmd_niveles_educativos').attachEvent('onItemClick', cmd_niveles_educativos_click)
        $$('emisor_cuenta_saldo_inicial').attachEvent('onChange', emisor_cuenta_saldo_inicial_change)
        $$('grid_emisor_cuentas_banco').attachEvent('onCheck', grid_emisor_cuentas_banco_on_check)
        //~ SAT
        tb_sat = $$('tab_sat').getTabbar()
        tb_sat.attachEvent('onChange', tab_sat_change)
        $$('grid_admin_taxes').attachEvent('onCheck', grid_admin_taxes_on_check)
        $$('grid_admin_taxes').attachEvent('onItemClick', grid_admin_taxes_click)
        $$('grid_admin_monedas').attachEvent('onCheck', grid_admin_monedas_on_check)
        $$('grid_admin_bancos').attachEvent('onCheck', grid_admin_bancos_on_check)
        $$('grid_admin_unidades').attachEvent('onCheck', grid_admin_unidades_on_check)
        $$('grid_admin_formasdepago').attachEvent('onCheck', grid_admin_formasdepago_on_check)
        $$('grid_admin_usos_cfdi').attachEvent('onCheck', grid_admin_usos_cfdi_on_check)
        $$('grid_unidad_found').attachEvent('onValueSuggest', grid_unidad_found_click)
        $$('grid_admin_unidades').attachEvent('onItemClick', grid_admin_unidades_click)
        $$('grid_moneda_found').attachEvent('onValueSuggest', grid_moneda_found_click)
        $$('cmd_agregar_impuesto').attachEvent('onItemClick', cmd_agregar_impuesto_click)
        //~ Usuarios
        $$('cmd_usuario_agregar').attachEvent('onItemClick', cmd_usuario_agregar_click)
        $$('grid_usuarios').attachEvent('onItemClick', grid_usuarios_click)
        $$('grid_usuarios').attachEvent('onCheck', grid_usuarios_on_check)
        $$('grid_usuarios').attachEvent('onItemDblClick', grid_usuarios_double_click)
        admin_ui_windows.init()

        //~ Opciones
        tb_options = $$('tab_options').getTabbar()
        tb_options.attachEvent('onChange', tab_options_change)
        $$('txt_plantilla_factura_32').attachEvent('onItemClick', txt_plantilla_factura_32_click)
        $$('txt_plantilla_factura_33').attachEvent('onItemClick', txt_plantilla_factura_33_click)
        $$('txt_plantilla_factura_33j').attachEvent('onItemClick', txt_plantilla_factura_33j_click)
        $$('txt_plantilla_ticket').attachEvent('onItemClick', txt_plantilla_ticket_click)
        $$('txt_plantilla_donataria').attachEvent('onItemClick', txt_plantilla_donataria_click)
        $$('txt_plantilla_nomina1233').attachEvent('onItemClick', txt_plantilla_nomina1233_click)
        $$('txt_plantilla_pagos10').attachEvent('onItemClick', txt_plantilla_pagos10_click)
        //~ Partners
        $$('chk_config_change_balance_partner').attachEvent('onItemClick', chk_config_item_click)

        $$('chk_config_ocultar_metodo_pago').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_ocultar_condiciones_pago').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_send_zip').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_open_pdf').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_show_pedimento').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_tax_locales').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_tax_decimals').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_price_with_taxes_in_invoice').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_add_same_product').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_tax_locales_truncate').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_decimales_precios').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_anticipo').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_ine').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_edu').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_pagos').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_cuenta_predial').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_codigo_barras').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_config_precio_con_impuestos').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_llevar_inventario').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_usar_punto_de_venta').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_ticket_pdf_show').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_ticket_direct_print').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_ticket_edit_cant').attachEvent('onItemClick', chk_config_item_click)
        $$('chk_ticket_total_up').attachEvent('onItemClick', chk_config_item_click)
        $$('txt_ticket_printer').attachEvent('onKeyPress', txt_ticket_printer_key_press)
        $$('txt_config_nomina_serie').attachEvent('onKeyPress', txt_config_nomina_serie_press)
        $$('txt_config_nomina_folio').attachEvent('onKeyPress', txt_config_nomina_folio_press)
        $$('txt_config_cfdipay_serie').attachEvent('onKeyPress', txt_config_cfdipay_serie_press)
        $$('txt_config_cfdipay_folio').attachEvent('onKeyPress', txt_config_cfdipay_folio_press)
        $$('chk_usar_nomina').attachEvent('onItemClick', chk_config_item_click)

        $$('cmd_subir_bdfl').attachEvent('onItemClick', cmd_subir_bdfl_click)
        $$('cmd_subir_cfdixml').attachEvent('onItemClick', cmd_subir_cfdixml_click)
        $$('up_bdfl').attachEvent('onUploadComplete', up_bdfl_upload_complete)
        $$('up_cfdixml').attachEvent('onUploadComplete', up_cfdixml_upload_complete)
    }
}


function menu_user_click(id, e, node){
    if (id == 1){
        window.location = '/logout'
        return
    }
}


function cmd_home_click(){
    window.location = '/main'
}


function cmd_save_emisor_click(){
    var valid_cp = false
    var form = $$('form_emisor')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    var ids = $$('lst_emisor_regimen').getSelectedId()
    if(!ids){
        msg = 'Selecciona al menos un Regimen Fiscal'
        msg_error(msg)
        return
    }
    if(values.emisor_cp && values.emisor_cp.length != 5){
        msg = 'Longitud inválida del C.P.'
        msg_error(msg)
        return
    }
    if(values.emisor_cp2 && values.emisor_cp2.length != 5){
        msg = 'Longitud inválida del C.P. de Expedición'
        msg_error(msg)
        return
    }else if(values.emisor_cp2){
        webix.ajax().sync().get('/values/cp', {cp: values.emisor_cp2}, {
            error: function(text, data, xhr) {
                msg = 'Error al consultar el C.P. de Expedición'
                msg_error(msg)
            },
            success: function(text, data, xhr) {
                var values = data.json();
                if (values.estado == undefined){
                    msg = 'No se encontró el C.P., asegurate de que sea correcto'
                    msg_error(msg)
                }else{
                    valid_cp = true
                }
            }
        })
    }
    if(!valid_cp){
        return
    }

    if(values.es_ong){
        if(!values.ong_autorizacion){
            msg = 'Si es ONG, el Número de Autorización del SAT es requerido'
            msg_error(msg)
            return
        }
        if(!values.ong_fecha){
            msg = 'Si es ONG, la Fecha de Autorización del SAT es requerida'
            msg_error(msg)
            return
        }
        if(!values.ong_fecha_dof){
            msg = 'Si es ONG, la Fecha de Publicación en DOF es requerida'
            msg_error(msg)
            return
        }
    }

    values['regimenes'] = ids
    webix.ajax().post('/emisor', values, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                msg_ok('Emisor guardado correctamente')
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function get_emisor(){
    var form = $$('form_emisor')

    webix.ajax().get("/emisor", {}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            if (values.ok){
                var emisor = values.row.emisor
                $$('lst_emisor_regimen').parse(values.row.regimenes)
                form.setValues(emisor, true)
                if(emisor.regimenes){
                    $$('lst_emisor_regimen').select(emisor.regimenes)
                }
                if(emisor.emisor_rfc.length == 12){
                    show('emisor_curp', false)
                }
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function get_certificado(){
    var form = $$('form_cert')

    webix.ajax().get("/values/cert", {}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            form.setValues(values)
        }
    })

}


function get_admin_cuentas_banco(){

    webix.ajax().get('/values/monedasid', function(text, data){
        var values = data.json()
        pre = values[0]
        $$('lst_emisor_cuenta_moneda').getList().parse(values)
        $$('lst_emisor_cuenta_moneda').setValue(pre.id)
    })

    webix.ajax().get('/values/bancosid', function(text, data){
        var values = data.json()
        pre = values[0]
        $$('lst_emisor_banco').getList().parse(values)
        if(values.length == 1){
            $$('lst_emisor_banco').setValue(pre.id)
        }
    })

    webix.ajax().get('/values/emisorcuentasbanco', {}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            $$('grid_emisor_cuentas_banco').parse(values)
        }
    })

}


function get_table_folios(){
    webix.ajax().get("/folios", {}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            $$('grid_folios').parse(values)
        }
    })

}


function get_config_correo(){
    var form = $$('form_correo')
    var fields = form.getValues()

    webix.ajax().get('/config', {'fields': 'correo'}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            form.setValues(values)
        }
    })
}


function get_admin_impuestos(){
    webix.ajax().sync().get('/values/alltaxes', function(text, data){
        var values = data.json()
        $$('grid_admin_taxes').clearAll()
        $$('grid_admin_taxes').parse(values, 'json')
    })
    $$('tab_sat').setValue('Impuestos')
}


function get_admin_monedas(){
    webix.ajax().sync().get('/values/allcurrencies', function(text, data){
        var values = data.json()
        $$('grid_admin_monedas').clearAll()
        $$('grid_admin_monedas').parse(values, 'json')
    })
}


function get_admin_bancos(){
    webix.ajax().sync().get('/values/allbancos', function(text, data){
        var values = data.json()
        $$('grid_admin_bancos').clearAll()
        $$('grid_admin_bancos').parse(values, 'json')
    })
}


function get_admin_unidades(){
    webix.ajax().sync().get('/values/allunidades', function(text, data){
        var values = data.json()
        $$('grid_admin_unidades').clearAll()
        $$('grid_admin_unidades').parse(values, 'json')
    })
}


function get_admin_formasdepago(){
    webix.ajax().sync().get('/values/allformasdepago', function(text, data){
        var values = data.json()
        $$('grid_admin_formasdepago').clearAll()
        $$('grid_admin_formasdepago').parse(values, 'json')
    })
}


function get_admin_usos_cfdi(){
    webix.ajax().sync().get('/values/allusoscfdi', function(text, data){
        var values = data.json()
        $$('grid_admin_usos_cfdi').clearAll()
        $$('grid_admin_usos_cfdi').parse(values, 'json')
    })
}


function get_admin_usuarios(){
    webix.ajax().sync().get('/values/allusuarios', function(text, data){
        var values = data.json()
        $$('grid_usuarios').clearAll()
        $$('grid_usuarios').parse(values, 'json')
    })
}


function set_config_templates(){
    webix.ajax().get('/config', {'fields': 'configtemplates'}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            Object.keys(values).forEach(function(key){
                show(key, values[key])
            })
        }
    })
}


function get_config_values(opt){
    if(opt == undefined){
        return
    }

    if(opt == 'templates'){
        set_config_templates()
    }

    webix.ajax().get('/config', {'fields': opt}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            //~ showvar(values)
            Object.keys(values).forEach(function(key){
                $$(key).setValue(values[key])
            })
        }
    })
}


function multi_admin_change(prevID, nextID){
    //~ webix.message(nextID)
    if(nextID == 'app_emisor'){
        $$('tab_emisor').setValue('Datos Fiscales')
        get_emisor()
        get_certificado()
        get_admin_cuentas_banco()
        return
    }

    if(nextID == 'app_folios'){
        get_table_folios()
        return
    }

    if(nextID == 'app_correo'){
        get_config_correo()
        return
    }

    if(nextID == 'app_sat'){
        get_admin_impuestos()
        return
    }

    if(nextID == 'app_usuarios'){
        get_admin_usuarios()
        return
    }

    if(nextID == 'app_options'){
        get_config_values('templates')
        return
    }
}


function emisor_postal_code_key_up(){
    var value = this.getValue()

    if( value.length == 5 ){
        webix.ajax().get('/values/cp', {cp: value}, {
            error: function(text, data, xhr) {
                msg = 'Error al consultar el C.P.'
                msg_error(msg)
            },
            success: function(text, data, xhr) {
                var values = data.json();
                if (values.estado == undefined){
                    msg = 'No se encontró el C.P., asegurate de que sea correcto'
                    msg_error(msg)
                } else {
                    $$('form_emisor').setValues({
                        emisor_cp2: value,
                        emisor_estado: values.estado,
                        emisor_municipio: values.municipio,
                        emisor_colonia: ''}, true)
                    $$('emisor_colonia').define('suggest', [])
                    if (webix.isArray(values.colonia)){
                        $$('emisor_colonia').define('suggest', values.colonia)
                    }else{
                        $$('form_emisor').setValues(
                            {emisor_colonia: values.colonia}, true)
                    }
                    $$('emisor_colonia').refresh()
                }
            }
        })
    }
}


function emisor_postal_code_key_press(code, e){
    var data = [8, 9, 37, 39, 46]
    if ( data.indexOf(code) >= 0 ){
        return true;
    }

    if ( code < 48 || code > 57){
        return false;
    }
}


function chk_escuela_change(new_value, old_value){
    var value = Boolean(new_value)
    if (value){
        $$('cmd_niveles_educativos').enable()
    } else {
        $$('cmd_niveles_educativos').disable()
    }
}


function chk_ong_change(new_value, old_value){
    var value = Boolean(new_value)
    if (value){
        $$('ong_autorizacion').enable()
        $$('ong_fecha').enable()
        $$('ong_fecha_dof').enable()
    } else {
        $$('ong_autorizacion').disable()
        $$('ong_fecha').disable()
        $$('ong_fecha_dof').disable()
    }
}


function cmd_subir_certificado_click(){
    var form = $$('form_upload')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    if(!values.contra.trim()){
        msg = 'La contraseña no puede estar vacía'
        msg_error(msg)
        return
    }

    if($$('lst_cert').count() < 2){
        msg = 'Selecciona al menos dos archivos: CER y KEY del certificado.'
        msg_error(msg)
        return
    }

    if($$('lst_cert').count() > 2){
        msg = 'Selecciona solo dos archivos: CER y KEY del certificado.'
        msg_error(msg)
        return
    }

    var fo1 = $$('up_cert').files.getItem($$('up_cert').files.getFirstId())
    var fo2 = $$('up_cert').files.getItem($$('up_cert').files.getLastId())

    var ext = ['key', 'cer']
    if(ext.indexOf(fo1.type.toLowerCase()) == -1 || ext.indexOf(fo2.type.toLowerCase()) == -1){
        msg = 'Archivos inválidos, se requiere un archivo CER y un KEY.'
        msg_error(msg)
        return
    }

    if(fo1.type == fo2.type && fo1.size == fo2.size){
        msg = 'Selecciona archivos diferentes: un archivo CER y un KEY.'
        msg_error(msg)
        return
    }

    var serie = $$('form_cert').getValues()['cert_serie']

    if(serie){
        msg = 'Ya existe un certificado guardado<BR><BR>¿Deseas reemplazarlo?'
        webix.confirm({
            title: 'Certificado Existente',
            ok: 'Si',
            cancel: 'No',
            type: 'confirm-error',
            text: msg,
            callback:function(result){
                if(result){
                    $$('up_cert').send()
                }
            }
        })
    }else{
        $$('up_cert').send()
    }
}


function up_cert_upload_complete(response){
    if(response.status != 'server'){
        msg = 'Ocurrio un error al subir los archivos'
        msg_error(msg)
        return
    }

    msg = 'Archivos subidos correctamente. Esperando validación'
    msg_ok(msg)

    var values = $$('form_upload').getValues()
    $$('form_upload').setValues({})
    $$('up_cert').files.data.clearAll()

    webix.ajax().post('/values/cert', values, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('form_cert').setValues(values.data)
                msg_ok(values.msg)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_agregar_serie_click(){
    var form = $$('form_folios')
    var grid = $$('grid_folios')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    var reg = /^[a-z]+$/i
    if(!reg.test(values.folio_serie)){
        msg = 'Introduce una serie válida. Solo letras.'
        msg_error(msg)
        return
    }

    var pre = '1'
    if(grid.count() > 0){
        pre = ''
    }

    var usarcon = ''
    if(values.folio_usarcon != 'S'){
        usarcon = values.folio_usarcon
    }

    var values = {
        serie: values.folio_serie.trim().toUpperCase(),
        inicio: values.folio_inicio,
        usarcon: usarcon,
        default: pre,
    }

    webix.ajax().post('/folios', values, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                form.setValues(
                    {folio_serie: '', folio_inicio: 1, folio_usarcon: 'S'})
                grid.add(values.row)
                msg = 'Serie agregada correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function grid_folios_click(id, e, node){
    if(id.column != 'delete'){
        return
    }

    msg = '¿Estás seguro de borrar esta serie?<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER'
    webix.confirm({
        title: 'Borrar Serie',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                webix.ajax().del('/folios', {id: id.row}, function(text, xml, xhr){
                    msg = 'Serie eliminada correctamente'
                    if(xhr.status == 200){
                        $$('grid_folios').remove(id.row)
                        msg_ok(msg)
                    }else{
                        msg = 'No se pudo eliminar'
                        msg_error(msg)
                    }
                })
            }
        }
    })
}


function validar_correo(values){

    if(!values.correo_servidor.trim()){
        msg = 'El servidor de salida no puede estar vacío'
        msg_error(msg)
        return false
    }
    if(!values.correo_puerto){
        msg = 'El puerto no puede ser cero'
        msg_error(msg)
        return false
    }
    if(!values.correo_usuario.trim()){
        msg = 'El nombre de usuario no puede estar vacío'
        msg_error(msg)
        return false
    }
    if(!values.correo_contra.trim()){
        msg = 'La contraseña no puede estar vacía'
        msg_error(msg)
        return false
    }
    if(!values.correo_asunto.trim()){
        msg = 'El asunto del correo no puede estar vacío'
        msg_error(msg)
        return false
    }
    if(!values.correo_mensaje.trim()){
        msg = 'El mensaje del correo no puede estar vacío'
        msg_error(msg)
        return false
    }

    return true
}


function cmd_probar_correo_click(){
    var form = $$('form_correo')
    var values = form.getValues()

    if(!validar_correo(values)){
        return
    }

    webix.ajax().sync().post('/values/correo', values, {
        error: function(text, data, xhr) {
            msg = 'Error al probar el correo'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Correo de prueba enviado correctamente. Ya puedes \
                    guardar esta configuración'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function save_config_mail(values){

    webix.ajax().sync().post('/config', values, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Configuración guardada correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function cmd_guardar_correo_click(){
    var form = $$('form_correo')
    var values = form.getValues()

    if(!validar_correo(values)){
        return
    }

    msg = 'Asegurate de haber probado la configuración<BR><BR>\
        ¿Estás seguro de guardar estos datos?'
    webix.confirm({
        title: 'Configuración de correo',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                save_config_mail(values)
            }
        }
    })
}


function emisor_logo_click(id, e){

    var w = webix.ui({
        view: 'window',
        id: 'win_emisor_logo',
        modal: true,
        position: 'center',
        head: 'Subir logotipo',
        body: {
            view: 'form',
            elements: body_win_emisor_logo,
        }
    })

    w.show()

    $$('up_emisor_logo').attachEvent('onUploadComplete', function(response){
        $$('emisor_logo').setValue(response.name)
        msg_ok('Logotipo cargado correctamente')
    })

}


function txt_plantilla_factura_33_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template', autosend: true, link: 'lst_files',
            value: 'Seleccionar archivo', upload: '/files/txt_plantilla_factura_33',
            width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla 3.3 ODT',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_factura_33').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function txt_plantilla_ticket_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template', autosend: true, link: 'lst_files',
            value: 'Seleccionar archivo', upload: '/files/txt_plantilla_ticket',
            width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla Ticket ODT',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_ticket').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function txt_plantilla_factura_32_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template', autosend: true, link: 'lst_files',
            value: 'Seleccionar archivo', upload: '/files/txt_plantilla_factura_32',
            width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla 3.2 ODT',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_factura_32').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function txt_plantilla_factura_33j_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template', autosend: true, link: 'lst_files',
            value: 'Seleccionar archivo', upload: '/files/txt_plantilla_factura_33j',
            width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla 3.3 JSON',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_factura_33j').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function txt_plantilla_donataria_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template', autosend: true, link: 'lst_files',
            value: 'Seleccionar archivo', upload: '/files/txt_plantilla_donataria',
            width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla Donataria',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_donataria').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function txt_plantilla_nomina1233_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template',
            autosend: true, link: 'lst_files', value: 'Seleccionar archivo',
            upload: '/files/txt_plantilla_nomina1233', width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla Nómina',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_nomina1233').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function txt_plantilla_pagos10_click(e){

    var body_elements = [
        {cols: [{width: 100}, {view: 'uploader', id: 'up_template',
            autosend: true, link: 'lst_files', value: 'Seleccionar archivo',
            upload: '/files/txt_plantilla_pagos10', width: 200}, {width: 100}]},
        {view: 'list',  id: 'lst_files', type: 'uploader', autoheight:true,
            borderless: true},
        {},
        {cols: [{}, {view: 'button', label: 'Cerrar', autowidth: true,
            click:("$$('win_template').close();")}, {}]}
    ]

    var w = webix.ui({
        view: 'window',
        id: 'win_template',
        modal: true,
        position: 'center',
        head: 'Subir Plantilla Factura de Pago',
        body: {
            view: 'form',
            elements: body_elements,
        }
    })

    w.show()

    $$('up_template').attachEvent('onUploadComplete', function(response){
        if(response.ok){
            $$('txt_plantilla_pagos10').setValue(response.name)
            msg_ok('Plantilla cargada correctamente')
        }else{
            msg_error(response.name)
        }
    })
}


function tab_options_change(nv, ov){
    var cv = {
        tab_admin_templates: 'templates',
        tab_admin_partners: 'partners',
        tab_admin_otros: 'configotros',
    }
    get_config_values(cv[nv])
}


function tab_sat_change(nv, ov){
    if(nv == 'Monedas'){
        get_admin_monedas()
    }else if(nv == 'Bancos'){
        get_admin_bancos()
    }else if(nv == 'Unidades'){
        get_admin_unidades()
    }else if(nv == 'Formas de Pago'){
        get_admin_formasdepago()
    }else if(nv == 'Usos de CFDI'){
        get_admin_usos_cfdi()
    }
}


function grid_admin_taxes_on_check(row, column, state){

    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/taxupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}


function grid_admin_monedas_on_check(row, column, state){

    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/currencyupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}


function grid_admin_bancos_on_check(row, column, state){

    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/bancoupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}


function grid_admin_unidades_on_check(row, column, state){

    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/unidadupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}


function grid_admin_formasdepago_on_check(row, column, state){

    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/formasdepagoupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}


function grid_admin_usos_cfdi_on_check(row, column, state){
    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/usocfdiupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}


function emisor_cuenta_saldo_inicial_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}


function cmd_emisor_agregar_cuenta_click(){
    var form = $$('form_emisor_cuenta_banco')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()
    var cuenta = {
        de_emisor: true,
        activa: true,
        nombre: values.emisor_cuenta_nombre.trim(),
        banco: values.emisor_banco,
        fecha_apertura: values.emisor_cuenta_fecha,
        fecha_deposito: values.emisor_fecha_saldo,
        cuenta: values.emisor_cuenta.trim(),
        clabe: values.emisor_clabe.trim(),
        moneda: values.emisor_cuenta_moneda,
        saldo_inicial: values.emisor_cuenta_saldo_inicial.to_float()
    }

    if(!cuenta.nombre){
        msg = 'El nombre de la cuenta es requerido'
        msg_error(msg)
        return
    }

    if(!cuenta.cuenta){
        msg = 'La cuenta es requerida'
        msg_error(msg)
        return
    }

    if(!cuenta.cuenta.is_number()){
        msg = 'Solo digitos en la cuenta'
        msg_error(msg)
        return
    }

    if(cuenta.cuenta.length < 9){
        msg = 'Longitud incorrecta de la cuenta'
        msg_error(msg)
        return
    }

    if(!cuenta.clabe){
        msg = 'La CLABE es requerida'
        msg_error(msg)
        return
    }

    if(cuenta.clabe.length != 18){
        msg = 'La CLABE debe ser de 18 digitos'
        msg_error(msg)
        return
    }

    if(!cuenta.clabe.is_number()){
        msg = 'Solo digitos en la CLABE'
        msg_error(msg)
        return
    }

    if(cuenta.saldo_inicial <= 0){
        msg = 'El saldo inicial no puede ser negativo o cero'
        msg_error(msg)
        return
    }

    cuenta.saldo = cuenta.saldo_inicial

    webix.ajax().post('/cuentasbanco', cuenta, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('grid_emisor_cuentas_banco').add(values.row)
                form.setValues({})
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function agregar_nueva_moneda(obj){
    var grid = $$('grid_admin_monedas')
    var values = {key: obj.key, name: obj.name}

    webix.ajax().post('/values/addmoneda', values, {
        error: function(text, data, xhr) {
            webix.message({type: 'error', text: 'Error al agregar'})
        },
        success: function(text, data, xhr){
            var values = data.json()
            if (values.ok){
                grid.add(obj)
            }
        }
    })
}


function grid_moneda_found_click(obj){
    msg = '¿Estás seguro de agregar la siguiente moneda?<BR><BR>'
    msg += '(' + obj.key + ') ' + obj.name

    webix.confirm({
        title: 'Agregar Moneda',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                agregar_nueva_moneda(obj)
            }
        }
    })
    $$('buscar_nueva_moneda').setValue('')
}


function agregar_nueva_unidad(obj){
    var grid = $$('grid_admin_unidades')
    var values = {key: obj.key, name: obj.name}

    webix.ajax().post('/values/addunidad', values, {
        error: function(text, data, xhr) {
            webix.message({type: 'error', text: 'Error al agregar'})
        },
        success: function(text, data, xhr){
            var values = data.json()
            if (values.ok){
                grid.add(obj)
            }
        }
    })
}


function grid_unidad_found_click(obj){
    msg = '¿Estás seguro de agregar la siguiente unidad?'
    msg += '(' + obj.key + ')<BR>'
    msg += obj.name

    webix.confirm({
        title: 'Agregar Unidad',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                agregar_nueva_unidad(obj)
            }
        }
    })
    $$('buscar_nueva_unidad').setValue('')
}


function agregar_impuesto(impuesto, tasa){
    var grid = $$('grid_admin_taxes')
    var values = {impuesto: impuesto, tasa: tasa}

    webix.ajax().post('/values/addimpuesto', values, {
        error: function(text, data, xhr) {
            webix.message({type: 'error', text: 'Error al agregar'})
        },
        success: function(text, data, xhr){
            var values = data.json()
            if (values.ok){
                $$('lst_admin_impuestos').setValue('')
                $$('txt_admin_tasa').setValue('')
                grid.add(values.row)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_agregar_impuesto_click(){
    var impuesto = $$('lst_admin_impuestos').getValue().trim()
    var tasa = $$('txt_admin_tasa').getValue().trim()

    if(!impuesto){
        msg = 'Selecciona un impuesto'
        msg_error(msg)
        return
    }
    if(!tasa){
        msg = 'Captura una tasa'
        msg_error(msg)
        return
    }
    if(!isFinite(tasa)){
        msg = 'La tasa debe ser un número'
        msg_error(msg)
        return
    }

    tasa = parseFloat(tasa)
    if(tasa >= 1){
        msg = 'La tasa debe ser menor a uno'
        msg_error(msg)
        return
    }

    msg = 'Datos correctos.<BR><BR>¿Estás seguro de agregar este impuesto?'
    webix.confirm({
        title: 'Agregar impuesto',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                agregar_impuesto(impuesto, tasa)
            }
        }
    })
}


function borrar_impuesto(row){
    var grid = $$('grid_admin_taxes')

    webix.ajax().del('/values/satimpuesto', {id: row}, function(text, xml, xhr){
        msg = 'Impuesto eliminado correctamente'
        if(xhr.status == 200){
            grid.remove(row)
            msg_ok(msg)
        }else{
            msg = 'Impuesto en uso, no se pudo eliminar.'
            msg_error(msg)
        }
    })
}


function grid_admin_taxes_click(id, e, node){
    if(id.column != 'delete'){
        return
    }

    msg = '¿Estás seguro de borrar el Impuesto seleccionado?'
    webix.confirm({
        title: 'Borrar Impuesto',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                borrar_impuesto(id.row)
            }
        }
    })

}


function delete_unit(id){
    var grid = $$('grid_admin_unidades')

    webix.ajax().del('/values/satunit', {id: id}, function(text, xml, xhr){
        msg = 'Unidad eliminada correctamente'
        if(xhr.status == 200){
            grid.remove(id)
            msg_ok(msg)
        }else{
            msg = 'Unidad en uso, no se pudo eliminar.'
            msg_error(msg)
        }
    })
}


function grid_admin_unidades_click(id, e, node){
    if(id.column != 'delete'){
        return
    }

    msg = '¿Estás seguro de borrar la Unidad seleccionada?'
    webix.confirm({
        title: 'Borrar Unidad',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                delete_unit(id.row)
            }
        }
    })

}


function eliminar_cuenta_banco(id){
    var grid = $$('grid_emisor_cuentas_banco')

    webix.ajax().del('/cuentasbanco', {id: id}, function(text, xml, xhr){
        msg = 'Cuenta eliminada correctamente'
        if(xhr.status == 200){
            grid.remove(id)
            msg_ok(msg)
        }else{
            msg = 'No se pudo eliminar'
            msg_error(msg)
        }
    })
}


function cmd_emisor_eliminar_cuenta_click(){
    var respuesta = undefined
    var row = $$('grid_emisor_cuentas_banco').getSelectedItem()

    if (row == undefined){
        msg = 'Selecciona una cuenta de banco'
        msg_error(msg)
        return
    }

    webix.ajax().sync().get('/values/ebancomov', {id: row['id']}, function(text, data){
        respuesta = data.json()
    })

    if(respuesta.ok){
        msg = 'La cuenta tiene movimientos, no se puede eliminar'
        msg_error(msg)
        return
    }

    var msg = '¿Estás seguro de eliminar la cuenta de banco?<BR><BR>'
    msg += row['banco'] + ' (' + row['cuenta'] + ')'
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER'
    webix.confirm({
        title: 'Eliminar Cuenta de Banco',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                eliminar_cuenta_banco(row['id'])
            }
        }
    })
}


function chk_config_item_click(id, e){
    var values = {}
    values[id] = $$(id).getValue()

    webix.ajax().sync().post('/config', values, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (!values.ok){
                msg_error(values.msg)
            }
        }
    })

}


function cmd_subir_bdfl_click(){
    var form = $$('form_upload_bdfl')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    if($$('lst_bdfl').count() < 1){
        msg = 'Selecciona la base de datos SQLite de Factura Libre'
        msg_error(msg)
        return
    }

    if($$('lst_bdfl').count() > 1){
        msg = 'Selecciona solo un archivo'
        msg_error(msg)
        return
    }

    var bdfl = $$('up_bdfl').files.getItem($$('up_bdfl').files.getFirstId())

    var ext = []
    if(bdfl.type.toLowerCase() != 'sqlite'){
        msg = 'Archivo inválido, se requiere un archivo SQLITE'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de subir este archivo?'
    webix.confirm({
        title: 'Base de datos de Factura Libre',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                $$('up_bdfl').send()
            }
        }
    })
}


function up_bdfl_upload_complete(response){
    if(response.status != 'server'){
        msg = 'Ocurrio un error al subir los archivos'
        msg_error(msg)
        return
    }

    msg = 'Archivo subido correctamente'
    msg_ok(msg)

    $$('form_upload_bdfl').setValues({})
    $$('up_bdfl').files.data.clearAll()
}


function cmd_subir_cfdixml_click(){
    var form = $$('form_upload_cfdixml')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    if($$('lst_cfdixml').count() < 1){
        msg = 'Selecciona al menos un archivo XML'
        msg_error(msg)
        return
    }

    if($$('lst_cfdixml').count() > 1){
        msg = 'Selecciona solo un archivo'
        msg_error(msg)
        return
    }

    var cfdixml = $$('up_cfdixml').files.getItem($$('up_cfdixml').files.getFirstId())

    //~ var ext = []
    if(cfdixml.type.toLowerCase() != 'xml'){
        msg = 'Archivo inválido, se requiere un archivo XML'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de subir este archivo?'
    webix.confirm({
        title: 'Importar CFDI',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                $$('up_cfdixml').send()
            }
        }
    })
}


function up_cfdixml_upload_complete(response){
    if(response.status != 'server'){
        msg = 'Ocurrio un error al subir los archivos'
        msg_error(msg)
        return
    }

    msg = 'Archivo importado correctamente'
    msg_ok(msg)

    $$('form_upload_cfdixml').setValues({})
    $$('up_cfdixml').files.data.clearAll()
}


function cmd_usuario_agregar_click(){
    var form  = $$('form_usuario')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    if(!values.usuario_usuario.trim()){
        msg = 'El campo Usuario no puede estar vacío'
        msg_error(msg)
        return
    }

    var rows = $$('grid_usuarios').data.getRange()
    for (i = 0; i < rows.length; i++) {
        if(rows[i]['usuario'] == values.usuario_usuario.trim()){
            msg = 'El usuario ya existe'
            msg_error(msg)
            return
        }
    }

    if(!values.usuario_contra1.trim()){
        msg = 'El campo Contraseña no puede estar vacío'
        msg_error(msg)
        return
    }

    if(values.usuario_contra1.trim().length < 5){
        msg = 'El campo Contraseña debe tener al menos 5 caracteres'
        msg_error(msg)
        return
    }

    if(!values.usuario_contra2.trim()){
        msg = 'El campo Confirmación de contraseña no puede estar vacío'
        msg_error(msg)
        return
    }

    if(values.usuario_contra1.trim() != values.usuario_contra2.trim()){
        msg = 'Las contraseñas no coinciden'
        msg_error(msg)
        return
    }

    var values = {
        usuario: values.usuario_usuario.trim(),
        contra: values.usuario_contra1.trim(),
    }

    msg = 'Datos correctos.<BR><BR>¿Estás seguro de agregar al nuevo usuario?'
    webix.confirm({
        title: 'Agregar Usuario',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                webix.ajax().post('/values/addusuario', values, {
                    error:function(text, data, XmlHttpRequest){
                        msg = 'Ocurrio un error, consulta a soporte técnico'
                        msg_error(msg)
                    },
                    success:function(text, data, XmlHttpRequest){
                        var values = data.json()
                        if(values.ok){
                            get_admin_usuarios()
                            msg_ok('Usuario agregado correctamente')
                        }else{
                            msg_error(values.msg)
                        }
                    }
                })
            }
        }
    })
}


function grid_usuarios_double_click(id, e, node){
    $$('win_edit_usuario').show()
}


function grid_usuarios_click(id, e, node){
    if(id.column != 'delete'){
        return
    }

    msg = '¿Estás seguro de borrar al usuario seleccionado?<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER'
    webix.confirm({
        title: 'Borrar Usuario',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                webix.ajax().del('/values/usuario', {id: id.row}, function(text, xml, xhr){
                    msg = 'Usuario eliminado correctamente'
                    if(xhr.status == 200){
                        $$('grid_usuarios').remove(id.row)
                        msg_ok(msg)
                    }else{
                        msg = 'No se pudo eliminar. Asegurate de no intentar autoeliminarte'
                        msg_error(msg)
                    }
                })
            }
        }
    })
}


function grid_usuarios_on_check(row, column, state){

    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/usuarioupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
            var values = data.json()
            if(values.ok){
                msg = 'Usuario actualizado correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function update_grid_usuarios(form, win){
    var values = form.getValues()

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    if(!values.usuario.trim()){
        focus('txt_usuario_usuario')
        msg = 'El usuario no puede estar vacío'
        msg_error(msg)
        return
    }
    if(!values.nombre.trim()){
        focus('txt_usuario_nombre')
        msg = 'El nombre no puede estar vacío'
        msg_error(msg)
        return
    }

    if(values.contra1.trim() != values.contra2.trim()){
        msg = 'Las contraseñas no coinciden'
        msg_error(msg)
        return
    }

    if(values.contra1.trim().length > 0 && values.contra1.trim().length < 5){
        msg = 'El campo Contraseña debe tener al menos 5 caracteres'
        msg_error(msg)
        return
    }
    if(values.correo.trim()){
        if(!validate_email(values.correo.trim())){
            msg = 'Correo inválido'
            msg_error(msg)
            return
        }
    }

    var values = {
        id: values.id,
        usuario: values.usuario.trim(),
        nombre: values.nombre.trim(),
        apellidos: values.apellidos.trim(),
        correo: values.correo.trim(),
        contra: values.contra1.trim(),
    }

    webix.ajax().post('/values/editusuario', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
            var values = data.json()
            if(values.ok){
                msg = 'Usuario actualizado correctamente'
                msg_ok(msg)
                form.save()
                win.hide()
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function txt_ticket_printer_key_press(code, e){
    var value = this.getValue()
    if(code != 13){
        return
    }

    if(!value){
        webix.ajax().del('/config', {id: 'txt_ticket_printer'}, function(text, xml, xhr){
            var msg = 'Impresora eliminada correctamente'
            if(xhr.status == 200){
                msg_ok(msg)
            }else{
                msg = 'No se pudo eliminar'
                msg_error(msg)
            }
        })
        return
    }

    webix.ajax().post('/config', {'txt_ticket_printer': value}, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Impresora guardada correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function txt_config_nomina_serie_press(code, e){
    var value = this.getValue()
    if(code != 13){
        return
    }

    if(!value.trim()){
        webix.ajax().del('/config', {id: 'txt_config_nomina_serie'}, function(text, xml, xhr){
            var msg = 'Serie de Nómina borrado correctamente'
            if(xhr.status == 200){
                msg_ok(msg)
            }else{
                msg = 'No se pudo eliminar'
                msg_error(msg)
            }
        })
        return
    }

    webix.ajax().post('/config', {'txt_config_nomina_serie': value.toUpperCase()}, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Serie de Nómina guardada correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function txt_config_nomina_folio_press(code, e){
    var value = this.getValue()
    if(code != 13){
        return
    }

    if(!value.trim()){
        webix.ajax().del('/config', {id: 'txt_config_nomina_folio'}, function(text, xml, xhr){
            var msg = 'Folio de Nómina borrado correctamente'
            if(xhr.status == 200){
                msg_ok(msg)
            }else{
                msg = 'No se pudo eliminar'
                msg_error(msg)
            }
        })
        return
    }

    if(!value.trim().is_number()){
        msg = 'El Folio de Nómina debe ser un número'
        msg_error(msg)
        return
    }

    webix.ajax().post('/config', {'txt_config_nomina_folio': value}, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Folio de Nómina guardado correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function txt_config_cfdipay_serie_press(code, e){
    var value = this.getValue()
    if(code != 13){
        return
    }

    if(!value.trim()){
        webix.ajax().del('/config', {id: 'txt_config_cfdipay_serie'}, function(text, xml, xhr){
            var msg = 'Serie de Pagos borrada correctamente'
            if(xhr.status == 200){
                msg_ok(msg)
            }else{
                msg = 'No se pudo eliminar'
                msg_error(msg)
            }
        })
        return
    }

    webix.ajax().post('/config', {'txt_config_cfdipay_serie': value.toUpperCase()}, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Serie de Pagos guardada correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function txt_config_cfdipay_folio_press(code, e){
    var value = this.getValue()
    if(code != 13){
        return
    }

    if(!value.trim()){
        webix.ajax().del('/config', {id: 'txt_config_cfdipay_folio'}, function(text, xml, xhr){
            var msg = 'Folio de Pagos borrado correctamente'
            if(xhr.status == 200){
                msg_ok(msg)
            }else{
                msg = 'No se pudo eliminar'
                msg_error(msg)
            }
        })
        return
    }

    if(!value.trim().is_number()){
        msg = 'El Folio de Pagos debe ser un número'
        msg_error(msg)
        return
    }

    webix.ajax().post('/config', {'txt_config_cfdipay_folio': value}, {
        error: function(text, data, xhr) {
            msg = 'Error al guardar la configuración'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                msg = 'Folio de Pagos guardado correctamente'
                msg_ok(msg)
            }else{
                msg_error(values.msg)
            }
        }
    })

}


function cmd_niveles_educativos_click(){
    admin_ui_niveles_educativos.init()
    $$('win_niveles_educativos').show()
    get_niveles_educativos()
}


function get_niveles_educativos(){
    webix.ajax().sync().get('/values/niveduall', {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            $$('grid_niveles_educativos').clearAll()
            $$('grid_niveles_educativos').parse(values)
            $$('grid_niveles_educativos').refresh()
        }
    })
}


function add_nivel_educativo_click(){
    var form = $$('form_niveles_educativos')

    if (!form.validate()){
        msg = 'Valores inválidos'
        msg_error(msg)
        return
    }

    var values = form.getValues()

    webix.ajax().post('/values/nivedu', values, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var result = data.json()
            form.setValues({})
            if(result.ok){
                $$('grid_niveles_educativos').add(values)
            }else{
                msg_error(result.msg)
            }
        }
    })

}


function delete_nivel_educativo(id){
    webix.ajax().del('/values/nivedu', {id: id}, function(text, xml, xhr){
        if(xhr.status == 200){
            $$('grid_niveles_educativos').remove(id)
        }else{
            msg = 'No se pudo eliminar'
            msg_error(msg)
        }
    })
}


function grid_niveles_educativos_click(id){
    if(id.column != 'delete'){
        return
    }

    msg = '¿Estás seguro de eliminar este Nivel Educativo'
    webix.confirm({
        title: 'Eliminar',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                delete_nivel_educativo(id.row)
            }
        }
    })

}


function grid_emisor_cuentas_banco_on_check(row, column, state){
    var values = {
        id: row,
        field: column,
        value: state,
    }
    webix.ajax().get('/values/emisorbancoupdate', values, {
        error: function(text, data, xhr) {
        },
        success: function(text, data, xhr) {
        }
    })
}
